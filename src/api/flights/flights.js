import { axiosInstance } from "api/api";

const apiRouters = {
  GetFlightList: "/flights",
  createFlightWithoutPhoto: "/flights",
  createFlightWithPhoto: "/flights/withPhoto",
  getFlightById: "/flights",
  checkFlightCode: "/flights/available",
  deleteFlight: "/flights",
  updateFlightWithoutPhoto: "/flights",
  updateFlightWithPhoto: "/flights",
  checkFlightsStatus: "/flights/status",
};

export const createFlightWithoutPhoto = async (payload) => {
  const response = await axiosInstance.post(
    apiRouters.createFlightWithoutPhoto,
    payload
  );
  return response;
};

export const createFlightWithPhoto = async (payload) => {
  const response = await axiosInstance.post(
    apiRouters.createFlightWithPhoto,
    payload
  );
  return response;
};

export const getFlightById = async (id) => {
  const response = await axiosInstance.get(
    `${apiRouters.getFlightById}/${id}/details`
  );
  return response;
};

export const checkFlightCode = async (id) => {
  const response = await axiosInstance.get(
    `${apiRouters.checkFlightCode}?code=${id}`
  );
  return response;
};

export const GetFlightList = async ({ page = 1, size = 10, code = null }) => {
  // Construct the base URL
  let url = `${apiRouters.GetFlightList}?page=${page}&size=${size}`;

  // Conditionally add the 'code' parameter if it's not null
  if (code) {
    url += `&code=${code}`;
  }
  // Send the GET request using Axios
  const response = await axiosInstance.get(url);
  return response;
};

export const deleteFlight = async (id) => {
  const response = await axiosInstance.delete(
    `${apiRouters.deleteFlight}/${id}`
  );
  return response;
};

export const updateFlightWithoutPhoto = async (id, payload) => {
  const response = await axiosInstance.put(
    `${apiRouters.updateFlightWithoutPhoto}/${id}`,
    payload
  );
  return response;
};

export const updateFlightWithPhoto = async (id, payload) => {
  const response = await axiosInstance.put(
    `${apiRouters.updateFlightWithPhoto}/${id}`,
    payload
  );
  return response;
};

export const checkFlightsStatus = async (ids) => {
  const params = new URLSearchParams();
  ids.forEach((id) => params.append("ids", id));
  const response = await axiosInstance.get(
    `${apiRouters.checkFlightsStatus}?${params.toString()}`
  );
  return response?.data;
};
