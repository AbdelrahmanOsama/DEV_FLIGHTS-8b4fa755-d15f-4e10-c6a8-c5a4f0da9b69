import { axiosExternalInstance } from "../external";

const apiAuth = {
  // Login
  login: "/auth/login",

  // Registration
  register: "/auth/register",
};

// Login
export const handleLogin = async (payload) => {
  const response = await axiosExternalInstance.post(
    `${apiAuth.login}`,
    payload
  );
  return response;
};

// Registration
export const makeRegistration = async (payload) => {
  const response = await axiosExternalInstance.post(
    `${apiAuth.register}`,
    payload
  );
  return response;
};
