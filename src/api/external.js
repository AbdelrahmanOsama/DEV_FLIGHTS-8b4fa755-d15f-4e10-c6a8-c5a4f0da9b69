import axios from "axios";
import { Base_URL } from "constants";
import { toast, Slide } from "react-toastify";

const Error = ({ errors }) => {
  return (
    <>
      <div className="flex flex-col gap-1">
        <span>{errors} *</span>
      </div>
    </>
  );
};
export default Error;

const axiosExternalInstance = axios.create({
  baseURL: Base_URL,
  timeout: 10000,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

axiosExternalInstance.interceptors.request.use(
  (config) => {
    config.timeout = 30 * 1000;
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// Add a response interceptor
axiosExternalInstance.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (error?.response?.status === 400) {
      // handle error message with toast library ...
      const error_label = error?.response?.data?.message;
      toast.error(<Error error={error_label} />, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        progress: undefined,
        theme: "colored",
        transition: Slide,
      });
    } else {
      toast.error(error?.message, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        progress: undefined,
        theme: "colored",
        transition: Slide,
      });
    }
    return Promise.reject(error);
  }
);

export { axiosExternalInstance };
