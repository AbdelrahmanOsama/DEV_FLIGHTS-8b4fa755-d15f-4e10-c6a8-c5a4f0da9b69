import axios from "axios";
import createAuthRefreshInterceptor from "axios-auth-refresh";
import { Base_URL } from "constants";
import { toast, Slide } from "react-toastify";
import { setUserInfo, setUserSession } from "redux/features/user/userSlice";
import { store } from "redux/store";

const Error = ({ error }) => {
  return (
    <>
      <div className="flex flex-col gap-1">
        <span>* {error}</span>
      </div>
    </>
  );
};
export default Error;

const axiosInstance = axios.create({
  baseURL: Base_URL,
  timeout: 10000,
  responseType: "json",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

axiosInstance.interceptors.request.use(
  (config) => {
    config.timeout = 30 * 1000;

    const session = localStorage.getItem("flightPortalSession");
    if (session) {
      config.headers = {
        authorization: `Bearer ${session}`,
        "accept-language": "en-US",
      };
    }

    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// Add a response interceptor
axiosInstance.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (error?.response?.status === 401) {
      localStorage.clear();
      window.location.href = "/";
      return Promise.reject(error);
    } else if (error?.response?.status === 400) {
      const error_label = error?.response?.data?.message;
      toast.error(<Error error={error_label} />, {
        position: "top-right",
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        progress: undefined,
        theme: "colored",
        transition: Slide,
        autoClose: 4000,
      });
      return Promise.reject(error);
    } else {
      toast.error(error?.message, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        progress: undefined,
        theme: "colored",
        transition: Slide,
      });
    }
    return Promise.reject(error);
  }
);

// Function that will be called to refresh authorization
export const refreshAuthLogic = (failedRequest) => {
  const user = store.getState()?.user?.user;
  return axios
    .post(`${Base_URL}/auth/refresh`, {
      refreshToken: user?.refreshToken,
    })
    .then((tokenRefreshResponse) => {
      store.dispatch(setUserSession(tokenRefreshResponse?.data?.token));
      store.dispatch(setUserInfo(tokenRefreshResponse?.data));
      setToken(tokenRefreshResponse?.data?.token);
      if (failedRequest) {
        failedRequest.response.config.headers.Authorization = `Bearer ${tokenRefreshResponse.data.token}`;
      }
      return Promise.resolve();
    })
    .catch(() => localStorage.clear());
};

export const setToken = (Token) => {
  if (Token) {
    axiosInstance.defaults.headers.common = {
      Authorization: `Bearer ${Token}`,
    };
  } else {
    delete axiosInstance.defaults.headers.common.Authorization;
  }
};

// Instantiate the interceptor
createAuthRefreshInterceptor(axiosInstance, refreshAuthLogic, {
  statusCodes: [403],
});

export { axiosInstance };
