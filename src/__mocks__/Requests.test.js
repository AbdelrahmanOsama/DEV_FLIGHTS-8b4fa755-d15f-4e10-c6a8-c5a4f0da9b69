import React from 'react';
import { render, screen, waitFor, fireEvent } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import '@testing-library/jest-dom/extend-expect';
import Requests from './Requests';
import { GetFlightList, deleteFlight } from 'api/flights/flights';

// Mock the API calls
jest.mock('api/flights/flights', () => ({
  GetFlightList: jest.fn(),
  deleteFlight: jest.fn(),
}));

jest.mock('utils/toast', () => ({
  __esModule: true,
  default: () => ({
    toast: jest.fn(),
  }),
}));

describe('Requests Component', () => {
  beforeEach(() => {
    GetFlightList.mockResolvedValue({
      data: {
        resources: [
          {
            id: 1,
            code: 'FL123',
            departureDate: '2023-12-01',
            status: 'Scheduled',
            capacity: 100,
            img: 'some-image-url',
          },
        ],
        total: 1,
      },
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('renders the Requests component with data', async () => {
    render(
        <Requests />
    );

    await waitFor(() => {
      expect(GetFlightList).toHaveBeenCalled();
    });

    expect(screen.getByText('Create Flight')).toBeInTheDocument();
    expect(screen.getByText('FL123')).toBeInTheDocument();
    expect(screen.getByText('01/12/2023')).toBeInTheDocument();
    expect(screen.getByText('Scheduled')).toBeInTheDocument();
  });

  test('opens and closes the delete confirmation modal', async () => {
    render(
      <Router>
        <Requests />
      </Router>
    );

    await waitFor(() => {
      expect(GetFlightList).toHaveBeenCalled();
    });

    const deleteButton = screen.getByTestId('DeleteForeverIcon');
    fireEvent.click(deleteButton);

    const modal = await screen.findByTestId('modal');
    expect(modal).toBeInTheDocument();
    expect(screen.getByText('Delete Flight Confirmation')).toBeInTheDocument();

    const closeButton = screen.getByText('Close');
    fireEvent.click(closeButton);
    await waitFor(() => {
      expect(modal).not.toBeInTheDocument();
    });
  });
});
