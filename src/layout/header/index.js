import React, { useRef } from "react";

import HeaderTitle from "./HeaderTitle";

import "./header.scss";

const Header = ({ setIsMenuOpen }) => {
  const nav = useRef(null);

  return (
    <div
      ref={nav}
      className="overlay-mobile navbar fixed top-0 justify-between"
    >
      <div>
        <div className="navbar-brand" to="/">
          <HeaderTitle />
        </div>
      </div>

      <div className="flex items-center gap-3">
        <div className="p-1 lg:flex hidden"></div>

        <div className="self-center lg:flex hidden"></div>

        <button
          className="navbar-toggler collapsed block lg:hidden"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarText"
          aria-controls="navbarText"
          aria-expanded="false"
          aria-label="Toggle navigation"
          onClick={() => setIsMenuOpen(true)}
        >
          <div className="hamburger flex gap-1">
            <div>
              <span className="line" />
              <span className="line" />
              <span className="line" />
            </div>
          </div>
        </button>
      </div>
    </div>
  );
};

export default Header;
