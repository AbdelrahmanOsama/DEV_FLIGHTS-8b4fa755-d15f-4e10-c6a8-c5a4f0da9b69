import React from "react";
import { useLocation } from "react-router-dom";

function HeaderTitle() {
  const location = useLocation();
  const { pathname } = location;
  const splitLocation = pathname.split("/");

  const items = [
    {
      path: "home",
      icon: (
        <i className="fa-solid fa-house text-white place-content-center"></i>
      ),
      title: "Flights",
    },
    {
      path: "flight",
      icon: (
        <i className="fa-solid fa-plane-departure text-white place-content-center"></i>
      ),
      title: "Flights",
    },
    {
      path: "flight",
      icon: (
        <i className="fa-solid fa-plane-departure text-white place-content-center"></i>
      ),
      title: "Flights",
    },
  ];

  return (
    <>
      <div className="flex flex-row items-center gap-3 group transition duration-500 ease-in-out visible_animation">
        <span
          className={`flex h-10 w-10 rounded-md group-hover:bg-primary-blue justify-center active:bg-primary-blue bg-primary-blue`}
        >
          {items.find((item) => item.path === splitLocation[1])?.icon}
        </span>
        <span
          id="headerTitle"
          className="font-bold sm:text-2xl text-base text-primary-dark text-left rtl:text-right text-exceed-limit-requests"
        />
      </div>
    </>
  );
}

export default HeaderTitle;
