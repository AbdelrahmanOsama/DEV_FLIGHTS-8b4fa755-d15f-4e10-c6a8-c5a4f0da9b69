import React from "react";
import Skeleton from "@mui/material/Skeleton";

export default function CustomSkeleton() {
  return (
    <div className="flex flex-row w-full ">
      <Skeleton
        sx={{ bgcolor: "rgba(0, 94, 184, .2)" }}
        animation="wave"
        variant="circular"
        width={56}
        height={56}
      />
      <div className="flex flex-1 flex-col mx-3 w-full gap-4 mt-1 ">
        <Skeleton
          sx={{ bgcolor: "rgba(0, 94, 184, .2)" }}
          animation="wave"
          variant="rounded"
          height={12}
          className=" h-3 w-10"
        />
        <Skeleton
          sx={{ bgcolor: "rgba(0, 94, 184, .2)" }}
          animation="wave"
          variant="rounded"
          height={16}
          className=" h-5 w-full"
        />
      </div>
    </div>
  );
}
