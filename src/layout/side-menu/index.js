import React, { useMemo } from "react";
import { useSelector } from "react-redux";
import classNames from "classnames";
import { Link, useLocation } from "react-router-dom";
import { AnimatePresence, motion } from "framer-motion";
import { useMediaQuery } from "react-responsive";

// Import the package.json file
import packageJson from "../../../package.json"; // Adjust the path as needed

// Assets
import logo from "assets/images/plane.png";
import home from "assets/images/home.svg";
import logoutIcon from "assets/images/logout.svg";

const Menu = ({ isMenuOpen, setIsMenuOpen }) => {
  const { user } = useSelector((state) => state.user);

  const location = useLocation();
  const isLaptop = useMediaQuery({ minWidth: 1024 });
  const { pathname } = location;
  const splitLocation = pathname.split("/");
  const activePath = splitLocation[1];

  const Project_Version = packageJson.version;

  const menuVariants = useMemo(
    () => ({
      active: {
        opacity: 1,
        x: 0,
      },
      inactive: {
        opacity: 0,
        x: "-100%",
      },
    }),
    []
  );

  const menuItems = useMemo(
    () => [
      {
        path: "home",
        icon: home,
        title: "Flights",
        show: true,
      },
    ],
    []
  );

  const logout = () => {
    localStorage.clear();
    window.location.href = "/";
  };

  return (
    <div className="visible_animation">
      <AnimatePresence>
        <motion.div
          key={"container"}
          animate={isLaptop ? "active" : isMenuOpen ? "active" : "inactive"}
          variants={menuVariants}
          transition={{ duration: 0 }}
          onClick={() => {
            setIsMenuOpen(false);
          }}
          className={`fixed overflow-hidden left-0 top-0 bottom-0 inset-0 z-50 lg:w-[300px] bg-primary-dark bg-opacity-25 lg:bg-transparent   lg:bg-opacity-100`}
        >
          <motion.div
            key="modal"
            animate={isLaptop ? "active" : isMenuOpen ? "active" : "inactive"}
            variants={menuVariants}
            transition={{ duration: 0.5 }}
            className=" fixed inset-0 overflow-auto overflow-y-scroll lg:overflow-y-auto h-full bg-white flex flex-col grow min-h-fit  lg:min-h-screen  gap-5 pt-8 pb-2 w-[300px]"
          >
            <div className="flex flex-col px-6 gap-4">
              <Link className="flex navbar-brand gap-4" to="/">
                <img className="w-8" src={logo} alt="Logo" />
                <p className="mb-0 self-center font-semibold">Just Flight</p>
              </Link>
              <div className="h-[1px] bg-primary-dark/10 w-full" />

              <div className="mb-0 flex gap-3 ">
                <i className="fa-solid fa-circle-user text-2xl text-primary-blue place-content-center"></i>
                <p className="mb-0 text-primary-blue self-center text-exceed-limit">
                  {user?.name}
                </p>
              </div>
              <div className="h-[1px] bg-primary-dark/10 w-full" />
            </div>

            <div className="flex flex-1 flex-col justify-between">
              <div className="flex flex-col gap-5">
                {menuItems
                  .filter((menuItem) => menuItem.show)
                  .map((item, index) => (
                    <Link
                      key={index}
                      to={`/${item.path}`}
                      className={`flex flex-row group items-center transition duration-500 ease-in-out   ${
                        activePath === item.path && " bg-[#f0f8ff]"
                      }`}
                      replace
                    >
                      <div
                        className={` h-14 w-[6px] rtl:rounded-l-md ltr:rounded-r-md ${
                          activePath === item.path && " bg-primary-blue "
                        }`}
                      />
                      <div className=" flex flex-row items-center px-6 ">
                        <img
                          src={item.icon}
                          alt={item.title}
                          className={classNames(
                            "h-7 w-7 m-auto group-hover:blue_icon",
                            {
                              blue_icon: activePath === item.path,
                            },
                            item?.iconClassName
                          )}
                        />
                        <h2
                          className={`text-lg font-bold group-hover:text-primary-blue active:text-primary-blue  mx-3 ${
                            activePath === item.path
                              ? "text-primary-blue"
                              : "text-primary-dark"
                          }`}
                        >
                          {item.title}
                        </h2>
                      </div>
                    </Link>
                  ))}
              </div>
              <div className="">
                <div className="h-[1px] bg-primary-dark/10 w-full my-4 " />
                <button className="flex flex-row items-center" onClick={logout}>
                  <span className="flex h-10 w-10 rounded-md ">
                    <img
                      src={logoutIcon}
                      alt={""}
                      className="h-6 w-6 m-auto ltr:rotate-180 contrast-0"
                    />
                  </span>
                  <span className="text-[#72747d] font-medium mx-1 ">
                    Logout
                  </span>
                </button>
              </div>
            </div>
            
            <div className="mx-6 my-2">
              <div className="h-[1px] bg-primary-dark/10 w-full my-4 " />
            </div>
            <div className="w-full text-center absolute bottom-12 md:bottom-3">
              <p className="text-[#72747d] text-xs">
                Version {Project_Version}
              </p>
            </div>
          </motion.div>
        </motion.div>
      </AnimatePresence>
    </div>
  );
};

export default Menu;
