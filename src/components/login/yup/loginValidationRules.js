import * as yup from "yup";

const loginValidationRules = yup.object().shape({
  email: yup
    .string()
    .trim("Invalid White Spaces")
    .strict(false)
    .required("Invalid Field")
    .test("test-name", "Invalid Email Format", function (value) {
      const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
      let isValidEmail = emailRegex.test(value);
      const validate = !isValidEmail ? false : true;
      return validate;
    }),
  password: yup.string().required("Invalid Field"),
});

export default loginValidationRules;
