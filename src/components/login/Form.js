import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import loginValidationRules from "./yup/loginValidationRules";
import Button from "../shared/Button";
import Input from "../shared/Input/Input";
import { handleLogin } from "../../api/auth";
import { setUserInfo, setUserSession } from "redux/features/user/userSlice";

export default function Form() {
  const [loading, setLoading] = useState(false);
  const [passwordVisible, togglePasswordVisible] = useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm({
    mode: "onSubmit",
    resolver: yupResolver(loginValidationRules),
  });

  const formSubmit = async ({ email, password }) => {
    try {
      setLoading(true);
      const res = await handleLogin({
        email: email,
        password: password,
      });
      // After Succuss login
      localStorage.setItem("flightPortalSession", res?.data?.token);
      dispatch(setUserSession(res?.data?.token));
      dispatch(setUserInfo(res?.data));
      navigate("/");
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  return (
    <>
      <form autoComplete="false" onSubmit={handleSubmit(formSubmit)}>
        <div className="mt-4">
          <Controller
            {...{
              control,
              name: "email",
              render: ({ field }) => (
                <Input
                  labelName="Email Address"
                  placeholder="Email Address"
                  required
                  containerClassName="w-full"
                  type="text"
                  error={errors.email}
                  id="email"
                  {...register("email", {
                    required: true,
                  })}
                  {...field}
                />
              ),
            }}
          />
        </div>

        <div className=" mt-10">
          <Controller
            {...{
              control,
              name: "password",
              render: ({ field }) => (
                <Input
                  labelName="Password"
                  placeholder="Password"
                  type={passwordVisible ? "text" : "password"}
                  error={errors.password}
                  id="password"
                  required
                  rightIcon={
                    <div>
                      <i
                        className={`fa-solid absolute z-[999999999999] text-gray-700 cursor-pointer ${
                          passwordVisible ? "fa-eye" : "fa-eye-slash"
                        }`}
                        onClick={(e) => togglePasswordVisible(!passwordVisible)}
                      ></i>
                    </div>
                  }
                  {...register("password", {
                    required: true,
                  })}
                  onPaste={(e) => {
                    e.preventDefault();
                    return false;
                  }}
                  {...field}
                />
              ),
            }}
          />
        </div>

        <div className=" mt-10">
          <Button
            label="Login"
            type="submit"
            isLoading={loading}
            classContainer={"w-full font-medium h-[53px] hover:primary-blue"}
          />
        </div>
        <div className="mt-8">
          <hr className="my-4" />
        </div>
        <div className=" mt-8">
          <Button
            label="Registration"
            classContainer={
              "w-full font-medium h-[53px] bg-primary-green hover:bg-primary-green"
            }
            onClick={() => navigate("/registration")}
          />
        </div>
      </form>
    </>
  );
}
