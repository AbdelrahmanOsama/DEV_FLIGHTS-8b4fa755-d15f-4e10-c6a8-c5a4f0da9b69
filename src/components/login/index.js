import React from "react";
import Form from "./Form";

export default function LoginForm() {
  return (
    <div className="flex flex-col justify-between h-full pt-4 w-full 2xl:w-[75%]">
      <div>
        <div className="mt-8 lg:mt-20 px-4 sm:px-0">
          <h3 className="login_header mb-4">Login</h3>
          <hr className="my-4" />
        </div>
        <div className="p-5 lg:p-[48px] bg-white rounded-0 sm:rounded-2xl">
          <Form />
        </div>
      </div>
    </div>
  );
}
