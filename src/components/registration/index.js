import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import Form from "./Form/Form";

import { handleLogin } from "../../api/auth";

export default function RegistrationForm() {
  const [formItems, setFormItems] = useState(null);
  const navigate = useNavigate();

  const login = async () => {
    try {
      const res = await handleLogin({
        email: formItems?.email,
        password: formItems?.password,
      });
      localStorage.setItem(
        "flightPortalSession",
        res?.data?.data?.access_token
      );
      navigate("/");
    } catch (error) {}
  };

  return (
    <div className="change-password flex flex-col justify-between h-full pt-4 w-full 2xl:w-[75%]">
      <div>
        <div className="mt-8 lg:mt-20 px-4 sm:px-0">
          <h3 className="login_header mb-3">Account Registration</h3>
          <p className="login_info mb-3">
            Join us to easily access and manage your flight information
          </p>
          <p className="login_info">
            <span>Do You have Account? </span>
            <span
              className="text-primary-green cursor-pointer"
              onClick={() => navigate("/login")}
            >
              Login
            </span>
          </p>
          <hr className="my-4" />
        </div>

        <div className="p-5 lg:p-[48px] bg-white rounded-0 sm:rounded-2xl mb-16">
          <Form setFormItems={setFormItems} onVerify={login} />
        </div>
      </div>
    </div>
  );
}
