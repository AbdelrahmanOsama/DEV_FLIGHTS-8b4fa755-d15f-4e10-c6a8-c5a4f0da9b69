import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Controller, useForm, useWatch } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";

// Components
import Button from "../../shared/Button";
import Input from "../../shared/Input/Input";
import BasicDetailsValidationRules from "./yup/RegistrationValidationRules";
import PasswordSteps from "./PasswordSteps/PasswordSteps";

// API
import { makeRegistration } from "api/auth";
import { setUserInfo, setUserSession } from "redux/features/user/userSlice";

export default function Form() {
  const [loading, setLoading] = useState(false);
  const [passwordVisible, togglePasswordVisible] = useState(false);
  const [passwordVisible2, togglePasswordVisible2] = useState(false);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm({
    mode: "onSubmit",
    resolver: yupResolver(BasicDetailsValidationRules),
  });

  const PasswordWatch = useWatch({
    control,
    name: "password",
  });

  const formSubmit = async ({ name, password, email }) => {
    try {
      setLoading(true);
      const payload = {
        name,
        password,
        email,
      };
      const res = await makeRegistration(payload);
      setLoading(false);
      localStorage.setItem("flightPortalSession", res?.data?.token);
      dispatch(setUserSession(res?.data?.token));
      dispatch(setUserInfo(res?.data));
      navigate("/");
    } catch (error) {
      console.log(error)
      setLoading(false);
    }
  };

  return (
    <>
      <form autoComplete="false" onSubmit={handleSubmit(formSubmit)}>
        <div className="mt-4">
          <div className="flex md:flex-row flex-col gap-4 mt-4">
            <Controller
              {...{
                control,
                name: "name",
                render: ({ field }) => (
                  <Input
                    labelName="Name"
                    placeholder="Name"
                    containerClassName="w-full"
                    type="text"
                    required
                    error={errors.name}
                    id="name"
                    {...register("name", {
                      required: true,
                    })}
                    {...field}
                  />
                ),
              }}
            />
          </div>
        </div>

        <div className=" mt-10">
          <Controller
            {...{
              control,
              name: "email",
              render: ({ field }) => (
                <Input
                  labelName="Email Address"
                  placeholder="Email Address"
                  required
                  containerClassName="w-full"
                  type="text"
                  error={errors.email}
                  id="email"
                  {...register("email", {
                    required: true,
                  })}
                  {...field}
                />
              ),
            }}
          />
        </div>

        <div className=" mt-10">
          <Controller
            {...{
              control,
              name: "password",
              render: ({ field }) => (
                <Input
                  labelName="Password"
                  placeholder="Password"
                  type={passwordVisible ? "text" : "password"}
                  error={errors.password}
                  required
                  id="password"
                  rightIcon={
                    <div>
                      <i
                        className={`fa-solid absolute text-gray-700 cursor-pointer ${
                          passwordVisible ? "fa-eye" : "fa-eye-slash"
                        }`}
                        onClick={(e) => togglePasswordVisible(!passwordVisible)}
                      ></i>
                    </div>
                  }
                  {...register("password", {
                    required: true,
                  })}
                  onPaste={(e) => {
                    e.preventDefault();
                    return false;
                  }}
                  {...field}
                />
              ),
            }}
          />
        </div>
        <div className="mt-3">
          <PasswordSteps PasswordWatch={PasswordWatch} />
        </div>
        <div className="mt-5">
          <Controller
            {...{
              control,
              name: "confirmPassword",
              render: ({ field }) => (
                <Input
                  labelName="Confirm Password"
                  placeholder="Confirm Password"
                  type={passwordVisible2 ? "text" : "password"}
                  error={errors.confirmPassword}
                  required
                  id="confirmPassword"
                  rightIcon={
                    <div>
                      <i
                        className={`fa-solid absolute text-gray-700 cursor-pointer ${
                          passwordVisible2 ? "fa-eye" : "fa-eye-slash"
                        }`}
                        onClick={(e) =>
                          togglePasswordVisible2(!passwordVisible2)
                        }
                      ></i>
                    </div>
                  }
                  {...register("confirmPassword", {
                    required: true,
                  })}
                  onPaste={(e) => {
                    e.preventDefault();
                    return false;
                  }}
                  {...field}
                />
              ),
            }}
          />
        </div>
        <div className="mt-10">
          <Button
            label="Sign up"
            type="submit"
            isLoading={loading}
            classContainer={"w-full h-[53px] hover:bg-[#094a8c]"}
          />
        </div>
      </form>
    </>
  );
}
