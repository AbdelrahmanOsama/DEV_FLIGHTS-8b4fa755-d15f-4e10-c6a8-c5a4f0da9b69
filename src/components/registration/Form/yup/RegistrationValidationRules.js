import * as yup from "yup";

// Adding validation to the form
const BasicDetailsValidationRules = yup.object().shape({
  name: yup
    .string()
    .trim("Invalid White Spaces")
    .strict(false)
    .min(2, "Min 2 characters")
    .max(35, "Max 35 characters")
    .required("Invalid Field"),

  email: yup
    .string()
    .trim("Invalid White Spaces")
    .strict(false)
    .required("Invalid Field")
    .test("test-name", "Invalid Email  Format", function (value) {
      const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
      let isValidEmail = emailRegex.test(value);
      const validate = !isValidEmail ? false : true;
      return validate;
    }),
  password: yup
    .string()
    .required("Invalid Field")
    .min(8, "Invalid Field")
    .matches(/^.*[A-Z].*$/, "Invalid Field")
    .matches(/[^a-zA-Z0-9 ]+/, "Invalid Field")
    .matches(/\d+.{1,}/, "Invalid Field"),
  confirmPassword: yup
    .string()
    .oneOf([yup.ref("password"), null], "Password not matching")
    .required("Invalid Field"),
});

export default BasicDetailsValidationRules;
