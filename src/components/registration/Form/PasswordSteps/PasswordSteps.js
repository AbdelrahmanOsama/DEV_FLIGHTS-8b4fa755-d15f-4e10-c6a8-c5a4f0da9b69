import styles from "./SetPassword.module.scss";

const PasswordSteps = ({ PasswordWatch }) => {

  const validIcon = () => {
    return (
      <i
        className={`fa-solid  ${styles.iconAligment} px-2 fa-circle text-[7px] text-[#40C1AC]`}
      ></i>
    );
  };

  const inValidIcon = () => {
    return (
      <i
        className={`fa-solid  ${styles.iconAligment} px-2 fa-circle text-[7px] text-[#90969E]`}
      ></i>
    );
  };

  return (
    <div className="passwordInst">
      <>
        <div className="gaps">
          {PasswordWatch?.length >= 8 ? validIcon() : inValidIcon()}
          <span className={`${PasswordWatch?.length >= 8 ? "text-[#40C1AC]" : "text-[#90969E]"} 2xl:text-lg text-xs`}>At least 8 characters long</span>
        </div>
        <div className="gaps">
          {PasswordWatch?.match(/[^a-zA-Z0-9 ]+/)
            ? validIcon()
            : inValidIcon()}
          <span className={`${PasswordWatch?.match(/[^a-zA-Z0-9 ]+/) ? "text-[#40C1AC]" : "text-[#90969E]"} 2xl:text-lg text-xs`}>At least one special character </span>
        </div>
        <div className="gaps">
          {PasswordWatch?.match(/[0-9]/) ? validIcon() : inValidIcon()}
          <span className={`${PasswordWatch?.match(/[0-9]/) ? "text-[#40C1AC]" : "text-[#90969E]"} 2xl:text-lg text-xs`}> Must contain at least one number </span>
        </div>
        <div className="gaps">
          {PasswordWatch?.match("^(.*?[A-Z]){1,}")
            ? validIcon()
            : inValidIcon()}
          <span className={`${PasswordWatch?.match("^(.*?[A-Z]){1,}") ? "text-[#40C1AC]" : "text-[#90969E]"} 2xl:text-lg text-xs`}> Must contain at least one number</span>
        </div>
      </>
    </div>
  );
};

export default PasswordSteps;
