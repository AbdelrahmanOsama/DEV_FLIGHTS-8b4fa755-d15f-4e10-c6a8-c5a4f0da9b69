import "./index.scss";
import plane from "../../../assets/images/plane.png"

const GlobalLoader = () => {
  return (
    <div className="loader__container fixed flex justify-center items-center">
      <img className="w-24" src={plane} alt="loading..." />
    </div>
  );
};
export default GlobalLoader;
