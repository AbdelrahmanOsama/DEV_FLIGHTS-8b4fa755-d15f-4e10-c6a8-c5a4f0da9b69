const LoadingSpinner = () => {
  return (
    <div className="text-center w-100">
      <div className={`loading__spinner`}></div>
    </div>
  );
};

export default LoadingSpinner;
