import { Pagination, PaginationItem } from "@mui/material";
import React from "react";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";

function CustomPagination({ itemsPerPage, currentPage, total, changePage }) {
  const currentPageCount =
    itemsPerPage * currentPage >= total ? total : itemsPerPage * currentPage;

  const currentStartItem = (currentPage - 1) * itemsPerPage + 1;

  function back() {
    return (
      <div className="flex place-items-center">
        <KeyboardArrowLeftIcon />
        <span className=" text-xs">Back</span>
      </div>
    );
  }

  function next() {
    return (
      <div className="flex place-items-center">
        <span className=" text-xs">Next</span>
        <KeyboardArrowRightIcon />
      </div>
    );
  }

  return (
    <>
      {Math.ceil(total / itemsPerPage) > 1 && (
        <div className="flex flex-col-reverse gap-4 sm:flex-row justify-between w-full">
          <div className="flex w-full place-items-center text-sm text-primary-dark justify-center sm:justify-start">
            {`Show page ${currentStartItem} – ${currentPageCount} of ${total}`}
          </div>

          <Pagination
            count={Math.ceil(total / itemsPerPage)}
            color="primary"
            shape="rounded"
            onChange={changePage}
            page={currentPage}
            renderItem={(item) => (
              <PaginationItem
                slots={{
                  previous: back,
                  next: next,
                }}
                {...item}
              />
            )}
            sx={{ width: "100%" }}
          />
        </div>
      )}
    </>
  );
}

export default CustomPagination;
