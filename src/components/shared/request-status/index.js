import React from "react";
import classNames from "classnames";

export default function RequestStatus({
  statusName,
  className = "py-2 ",
  classContainer = "",
}) {
  const handleStatusColor = () => {
    if (statusName === "processing") {
      return "#EE7501";
    } else if (statusName === "ready") {
      return "#167767";
    } else {
      return "#F4F6F9";
    }
  };

  const handleStatusBG = () => {
    if (statusName === "processing") {
      return "rgba(238, 117, 1, 0.1)";
    } else if (statusName === "ready") {
      return "rgba(64, 193, 172, 0.10)";
    } else {
      return "#8FA0B4";
    }
  };

  return (
    <div
      className={classNames(
        "flex gap-1 items-center",
        classContainer
      )}
    >
      <div
        className={classNames("rounded h-fit px-3 text-center", className)}
        style={{ backgroundColor: handleStatusBG() }}
      >
        <span className="text-xs" style={{ color: handleStatusColor() }}>
          {statusName === "processing" && (
            <i className="fa-solid fa-spinner fa-spin text-[#EE7501] mr-2"></i>
          )}
          <span className="capitalize ">{statusName}</span>
        </span>
      </div>
    </div>
  );
}
