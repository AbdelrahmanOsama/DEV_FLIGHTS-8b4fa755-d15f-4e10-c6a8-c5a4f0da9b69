import React from "react";
import info from "../../../assets/images/error-input.svg";

import "./errorInput.scss";

const ErrorInput = ({ message }) => {
  return (
    <span className="error-input">
      <img src={info} alt="info" style={{ margin: "0 5px 0 5px" }} />
      {message}
    </span>
  );
};

export default ErrorInput;
