import * as React from "react";

import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";
import icon from "../../../assets/icons/comment-info-alt.svg";

const CustomModal = ({
  title,
  description,
  open,
  link = null,
  onClose,
  showCloseBtn = true,
}) => {
  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      open={open}
      className="flex items-center justify-center"
    >
      <Fade in={open}>
        <div className="relative bg-white flex  flex-col items-center gap-8 py-10 px-8 sm:px-12 md:px-20 mx-6 rounded-lg max-w-4xl ">
          <img src={icon} alt="warning" className="h-24 w-24" />

          <div className="flex flex-col items-center gap-4">
            <h2 className=" text-xl md:text-2xl text-center font-bold text-primary-blue  ">
              {title}
            </h2>
            <p className="md:text-lg text-primary-dark text-center">
              {description}
            </p>

            {link()}
          </div>

          {showCloseBtn && (
            <button
              onClick={onClose}
              className=" absolute rtl:left-3 ltr:right-3 top-3 "
            >
              <CloseRoundedIcon
                sx={{ fontSize: "32px" }}
                className="m-2 text-primary-dark/60 "
              />
            </button>
          )}
        </div>
      </Fade>
    </Modal>
  );
};

export default CustomModal;
