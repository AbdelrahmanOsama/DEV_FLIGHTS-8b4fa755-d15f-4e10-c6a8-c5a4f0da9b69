import { Skeleton } from "@mui/material";
import React from "react";

function ServiceSkeleton() {
  return (
    <Skeleton
      sx={{ bgcolor: "rgba(0, 94, 184, .2)" }}
      animation="wave"
      variant="rounded"
      className="flex flex-1 !h-40  xs:!h-56  "
    />
  );
}

export default ServiceSkeleton;
