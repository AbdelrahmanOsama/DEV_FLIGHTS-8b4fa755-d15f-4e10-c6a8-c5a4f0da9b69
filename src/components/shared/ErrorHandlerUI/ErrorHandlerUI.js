import React from "react";
import { getI18n, useTranslation } from "react-i18next";

const Error = ({ errors }) => {
  const { language } = getI18n();
  const errorsArr =
    language === "ar" ? errors?.arabicMessages : errors?.englishMessages;
  return (
    <>
      {errorsArr?.map((err, index) => (
        <div key={index} className="flex flex-col gap-1">
          <span>- {err}</span>
        </div>
      ))}
    </>
  );
};

const ErrorHandlerUI = ({ errors }) => {
  const { t } = useTranslation();
  return (
    <>
      {errors && (
        <div className="bg-[#fff] bg-opacity-70 px-6 rounded-[4px] py-2 rtl:border-r-2 ltr:border-l-2 border-[#ee73019c]">
          <h3 className="text-[#ee7501] font-medium pb-2">{t("error")}</h3>
          <div className="text-[#ee73019c]">
            <Error errors={errors} />
          </div>
        </div>
      )}
    </>
  );
};

export default ErrorHandlerUI;
