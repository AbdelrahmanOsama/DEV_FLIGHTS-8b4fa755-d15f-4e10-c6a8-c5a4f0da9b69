import React from "react";

const EmptyContent = ({ title, description, icon }) => {
  return (
    <div className="flex flex-1 flex-col grow w-full gap-4 self-center items-center justify-center  py-12 px-8  ">
      {icon && <img src={icon} className="h-14 w-14 self-center" />}
      <h1 className="text-primary-dark text-lg font-bold text-center ">
        {title}
      </h1>
      <p className="text-primary-dark/70  text-center">{description}</p>
    </div>
  );
};

export default EmptyContent;
