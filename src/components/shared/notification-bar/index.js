import React from "react";
import { useTranslation } from "react-i18next";

import "./index.scss";

function NotificationBar() {
  const { t } = useTranslation();
  return (
    <div class="notification-top-bar">
      <p>
        {t("noticeDesc")}
        <small>
          <a href="https://www.ia.gov.sa/" target="_blank">
            www.ia.gov.sa
          </a>
        </small>
        {t("noticeNum")}
      </p>
    </div>
  );
}

export default NotificationBar;
