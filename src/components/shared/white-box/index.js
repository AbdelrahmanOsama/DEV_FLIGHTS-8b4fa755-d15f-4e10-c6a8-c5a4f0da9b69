import React from "react";

export default function WhiteBox({ children, className }) {
  return (
    <div
      className={`h-full p-4 sm:p-6 md:p-8 flex flex-col gap-6 bg-white md:rounded-xl items-start ${className}`}
    >
      {children}
    </div>
  );
}
