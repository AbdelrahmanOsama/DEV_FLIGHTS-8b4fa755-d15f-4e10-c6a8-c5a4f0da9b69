import React from "react";
import PictureAsPdfRoundedIcon from "@mui/icons-material/PictureAsPdfRounded";

function FileExtensionIcon() {
  return (
    <PictureAsPdfRoundedIcon
      sx={{
        fontSize: "2rem",
      }}
      className=" text-4xl text-primary-blue"
    />
  );
}
export default FileExtensionIcon;
