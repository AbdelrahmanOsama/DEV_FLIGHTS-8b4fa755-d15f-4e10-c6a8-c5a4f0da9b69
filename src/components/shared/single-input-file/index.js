import React, { useState } from "react";
import Dropzone from "react-dropzone";
import classNames from "classnames";

// Material UI
import CloudUploadOutlinedIcon from "@mui/icons-material/CloudUploadOutlined";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import VisibilityIcon from "@mui/icons-material/Visibility";
import InsertDriveFileIcon from "@mui/icons-material/InsertDriveFile";

// Helpers
import useToast from "utils/toast";

// Components
import CircularLoader from "../circular-loader";
import ErrorInput from "../ErrorInput/ErrorInput";

// Styles
import "./styles.scss";
import CustomizedDialogs from "../Modal/Modal";

const SingleInputFile = React.forwardRef(
  (
    {
      label,
      value,
      url,
      error,
      allowedExtension,
      disabled,
      inProgress,
      required,
      onChange,
      handleDelete,
      ...rest
    },
    ref
  ) => {
    const [loading, setLoading] = useState(false);
    const [show, setShow] = useState(false);
    const [previewUrl, setPreviewUrl] = useState(url || null);
    const { toast } = useToast();

    const handleDrop = async (acceptedFiles) => {
      const maxSize = 5 * 1024 * 1024; // 5MB
      const fileSize = acceptedFiles[0].size;

      if (fileSize > maxSize) {
        toast("warn", "The maximum file size is 5MB.");
        return;
      }
      const preview = URL.createObjectURL(acceptedFiles[0]);
      setPreviewUrl(preview);
      setLoading(true);
      inProgress("uploading");

      try {
        await onChange(acceptedFiles);
        inProgress("done");
      } catch (error) {
        toast("error", "File upload failed");
      } finally {
        setLoading(false);
      }
    };

    const onDeleteFile = () => {
      handleDelete(value?.attachmentId);
    };

    const extractFileName = () => {
      let fileName = null;

      value.forEach((value, key) => {
        if (value instanceof File) {
          fileName = value.name;
        }
      });

      return fileName;
    };

    const fileName = extractFileName(value);
    return (
      <div className="flex flex-col gap-2">
        <div className=" flex flex-col gap-2 ">
          <label
            className={classNames("input-label", {
              "text-red": error && !loading,
            })}
          >
            {label}
            {required && (
              <span className="text-red-900 text-sm ml-[2px]">*</span>
            )}
          </label>
          <div className="flex flex-col ">
            <div
              className={classNames(
                "flex flex-row bg-white h-[56px]  border-2 rounded border-[#00255433]",
                {
                  "border-red": error && !loading,
                }
              )}
            >
              <Dropzone
                onDrop={handleDrop}
                multiple={false}
                disabled={loading || disabled}
                accept={{
                  "image/png": [".png", ".jpg", ".jpeg"],
                }}
                className="h-full flex flex-1 overflow-hidden"
              >
                {({ getRootProps, getInputProps }) => (
                  <div
                    className="h-full w-full overflow-hidden"
                    {...getRootProps()}
                  >
                    <input {...getInputProps()} />

                    {loading ? (
                      <div className="h-full w-full flex items-center justify-center  ">
                        <CircularLoader />
                      </div>
                    ) : fileName ? (
                      <div className="flex h-full flex-row px-4 flex-1   gap-3 items-center justify-start ">
                        <InsertDriveFileIcon sx={{ color: "#005EB8" }} />
                        <div className="flex flex-col flex-1 overflow-hidden gap-1">
                          <p className="text-primary-dark text-sm font-medium oneLine  ">
                            {fileName}
                          </p>
                        </div>
                      </div>
                    ) : (
                      <div className="flex flex-row h-full self-center items-center justify-center gap-3">
                        <p className="pt-1 flex flex-1  flex-wrap px-3 ">
                          <span className=" text-sm font-medium text-primary-dark/50 ">
                            Drag and drop file here or
                            <strong className=" text-primary-blue font-bold ">
                              {"  "} Browse
                            </strong>
                          </span>
                        </p>
                        {!disabled && (
                          <div className="h-full px-6 bg-primary-grey/10 flex items-center justify-center">
                            <CloudUploadOutlinedIcon
                              sx={{ fontSize: "32px" }}
                              className="text-primary-grey  "
                            />
                          </div>
                        )}
                      </div>
                    )}
                  </div>
                )}
              </Dropzone>

              {fileName && !loading && (
                <div className="flex h-full flex-row items-center gap-[1px] ">
                  <div
                    onClick={() => setShow(true)}
                    className="h-full w-14  bg-primary-grey/10  flex items-center justify-center cursor-pointer "
                  >
                    <VisibilityIcon className=" text-primary-blue" />
                  </div>
                  {!disabled && (
                    <button
                      onClick={onDeleteFile}
                      className="h-full w-14  bg-primary-grey/10  flex items-center justify-center "
                    >
                      <DeleteOutlineIcon className=" text-primary-red" />
                    </button>
                  )}
                </div>
              )}
            </div>

            <p className="text-xs text-primary-dark/60 m-2">
              {`Accepted Files: ${allowedExtension?.join(
                ", "
              )} -  max size  : 5MB`}
            </p>
            {error && !loading && <ErrorInput message={error?.message} />}
          </div>
        </div>

        <CustomizedDialogs
          show={show}
          handleClose={() => setShow(false)}
          title="Image Preview"
        >
          <div className="flex flex-col items-center gap-4">
            <img
              src={previewUrl}
              alt="preview"
              className="w-full object-cover rounded-lg"
            />
            <button
              className="bg-primary-blue text-white px-4 py-2 rounded-lg"
              onClick={() => setShow(false)}
            >
              Close
            </button>
          </div>
        </CustomizedDialogs>
      </div>
    );
  }
);

export default SingleInputFile;
