import React from "react";
import ArrowBackRoundedIcon from "@mui/icons-material/ArrowBackRounded";
import { useNavigate } from "react-router-dom";

export default function BackSection({ title }) {
  const navigate = useNavigate();
  const goBack = () => {
    navigate(-1);
  };
  return (
    <div className="w-full bg-white lg:rounded-xl">
      <button
        onClick={goBack}
        className="px-4 py-5 flex flex-row gap-5 items-center"
      >
        <ArrowBackRoundedIcon
          fontSize="small"
          className="text-primary-green  rtl:rotate-180"
        />
        <span className=" text-primary-dark">{title}</span>
      </button>
    </div>
  );
}
