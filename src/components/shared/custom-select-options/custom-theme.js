export default {
  indicatorSeparator: () => ({ display: "none" }),
  control: (base) => ({
    ...base,
    border: "none",
    borderRadius: "4px",
    minHeight: "44px",
    fontSize: "14px",
    paddingLeft: "6px",
  }),
  menuPortal: (base) => ({ ...base, zIndex: 9999 }),
  menuList: (base) => ({
    ...base,
    display: "flex",
    flexDirection: "column",
    gap: "8px",
    padding: "8px 14px",
    backgroundColor: "#F4F6F9",
    borderRadius: "4px",
    maxHeight: "500px",
  }),
  option: (base, state) => ({
    ...base,
    borderRadius: "6px",
    backgroundColor: state.isSelected ? "#005EB8" : "#fff",
    color: state.isSelected ? "#ffffff" : "#002554",
    padding: "0px",
    display: "flex",
    "&:hover": {
      backgroundColor: "#005EB8",
      color: "#ffffff",
    },
  }),
};
