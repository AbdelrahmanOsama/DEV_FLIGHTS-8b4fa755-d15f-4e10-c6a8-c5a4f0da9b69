import { components } from "react-select";

const DefaultOption = (props) => {
  return (
    <components.Option className="autoHeight" {...props}>
      <span className="option-name py-2">{props.label}</span>
    </components.Option>
  );
};

export default DefaultOption;
