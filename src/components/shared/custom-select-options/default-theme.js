export default {
  indicatorSeparator: () => ({ display: "none" }),
  control: (base) => ({
    ...base,
    border: "none",
    borderRadius: "4px",
    minHeight: "44px",
    fontSize: "14px",
    paddingLeft: "6px",
  }),
  menuPortal: (base) => ({ ...base, zIndex: 9999 }),
  option: (base, state) => ({
    ...base,
    backgroundColor: state.isSelected && "#F4F4F4",
    borderBottom: "1px solid #f4f4f4",
    display: "flex",
    alignItems: "center",
    color: "#333",
    "&:hover": {
      backgroundColor: "#F4F4F4",
    },
  }),
};
