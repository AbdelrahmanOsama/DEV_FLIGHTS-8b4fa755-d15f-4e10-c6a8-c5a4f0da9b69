import React, { useMemo } from "react";
import classNames from "classnames";
import Select from "react-select";
import { CircularProgress } from "@mui/material";
import { useTranslation } from "react-i18next";

import ErrorInput from "../ErrorInput/ErrorInput";

import CustomOption from "./custom-option";
import DefaultOption from "./default-option";

import customTheme from "./custom-theme";
import defaultTheme from "./default-theme";

import "./styles.scss";

const CustomSelectOptions = React.forwardRef(
  (
    {
      name = "",
      placeholder = "",
      labelName = "",
      value,
      onChange,
      required = false,
      loading,
      error,
      disabled,
      options = [],
      defaultValue = null,
      isCustom = false,
      ...rest
    },
    ref
  ) => {
    const { t, i18n } = useTranslation();

    const selectedValue = useMemo(() => {
      const item = options?.find((option) => `${option.value}` === `${value}`);
      return item
        ? {
            value,
            label: i18n.language === "ar" ? item?.nameAr : item?.nameEn,
          }
        : null;
    }, [value, i18n.language]);

    return (
      <div className="flex flex-col gap-2">
        <label
          className={classNames("input-label", {
            "text-red": error,
          })}
        >
          {labelName}
          {required && <span className="text-red-900 text-sm ml-[2px]">*</span>}
        </label>
        <div className="input-relative">
          {loading ? (
            <div className="flex justify-center place-items-center h-[50px]">
              <CircularProgress size={30} />
            </div>
          ) : (
            <div className="relative select">
              <Select
                className="select-overlay"
                classNamePrefix="select"
                menuPortalTarget={document?.body}
                placeholder={placeholder}
                styles={isCustom ? customTheme : defaultTheme}
                components={{
                  Option: isCustom ? CustomOption : DefaultOption,
                }}
                name={name}
                options={options}
                value={selectedValue || defaultValue}
                indicator
                onChange={(newValue) => {
                  if (newValue?.value != value) {
                    onChange(newValue);
                  }
                }}
                ref={ref}
                isDisabled={disabled}
                noOptionsMessage={() => t("no_options")}
                {...rest}
              />
              <fieldset
                aria-hidden="true"
                className={classNames("", {
                  "border-red": error,
                })}
              >
                <legend
                  className={classNames({
                    "border-red": error,
                    "border-trans": value,
                  })}
                >
                  <span>{placeholder}</span>
                </legend>
              </fieldset>
            </div>
          )}
        </div>
        {error && <ErrorInput message={error?.message} />}
      </div>
    );
  }
);

export default CustomSelectOptions;
