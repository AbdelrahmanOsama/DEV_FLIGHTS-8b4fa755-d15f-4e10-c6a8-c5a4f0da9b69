import { components } from "react-select";

const CustomOption = (props) => {
  return (
    <components.Option className="autoHeight group" {...props}>
      <div className={`flex flex-row p-3 gap-4 items-center`}>
        <div className="  flex items-center justify-center h-16 w-16 bg-main-grey rounded-md  ">
          {props?.data?.icon && (
            <img
              src={props?.data?.icon}
              alt="insuranceCompany"
              className="w-16 h-16"
            />
          )}
        </div>
        <span
          className={`w-[1px] h-9 bg-primary-dark/10  ${
            props.isSelected ? "!bg-main-grey" : "!bg-primary-dark/10 "
          } group-hover:!bg-main-grey`}
        />
        <span
          className={` text-sm font-medium ${
            props.isSelected ? "!text-white" : "!text-primary-dark "
          } group-hover:!text-white`}
        >
          {props?.label}
        </span>
      </div>
    </components.Option>
  );
};

export default CustomOption;
