import React from "react";
import classNames from "classnames";
import { useTranslation } from "react-i18next";
import { CircularProgress } from "@mui/material";

export default function Button({
  label = "",
  iconComponent = null,
  iconDirection = "left",
  isLoading,
  outline = false,
  classContainer = "",
  labelClassContainer = "",
  handleAction = () => "",
  ...props
}) {
  const { t } = useTranslation();
  return (
    <button
      onClick={handleAction}
      className={classNames(
        { "border-[1px] border-[#8FA0B4] bg-transparent": outline },
        { "bg-primary-blue": !outline },
        "flex items-center justify-center gap-2 rounded-md h-9 transition duration-300 ease-in-out px-5",
        { "!bg-primary-gray": props.disabled },
        classContainer
      )}
      {...props}
      disabled={isLoading || props.disabled}
    >
      {iconDirection === "right" && iconComponent}
      {isLoading ? (
        <CircularProgress size={30} sx={{ color: "white" }} />
      ) : (
        label && (
          <span
            className={classNames(
              { "text-[#8FA0B4]": outline },
              { "text-white": !outline },
              "text-[12px] sm:text-[14px] font-medium",
              labelClassContainer
            )}
          >
            {t(label)}
          </span>
        )
      )}
      {iconDirection === "left" && iconComponent}
    </button>
  );
}
