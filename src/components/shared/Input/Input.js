import classNames from "classnames";
import React from "react";
import isEqual from "lodash/isEqual";

import ErrorInput from "../ErrorInput/ErrorInput";

import "./input.scss";

const Input = React.forwardRef(
  (
    {
      containerClassName = "",
      className = "border-transparent bg-transparent px-3 h-[56px]",
      labelClassName = "",
      labelName = "",
      inputRelativeClassName = "",
      type = "text",
      id = "",
      error = "",
      required = false,
      leftIcon = null,
      rightIcon = null,
      isTextArea = false,
      isDisabled = false,
      value = "",
      handleBlur,
      ...rest
    },
    ref
  ) => {
    const [isActive, setActive] = React.useState(
      isEqual(value, 0) && type === "number" ? true : !!value
    );


    const setInputFocus = (status) =>
      value ? setActive(true) : setActive(status);

    React.useEffect(() => {
      // fix Google Chrome autofill
      setTimeout(() => {
        // fetch input
        const autoFillInput = document.querySelector("input:-webkit-autofill");
        // detect input change
        if (autoFillInput) {
          setInputFocus(true);
        }
      }, 500);
    }, [setInputFocus]);

    React.useEffect(() => {
      setActive(value === 0 && type === "number" ? true : !!value);
    }, [value, type]);

    const renderInput = () =>
      isTextArea ? (
        <textarea
          type={type}
          id={id}
          ref={ref}
          value={value}
          {...rest}
          className={classNames(
            "input-field h-auto min-h-[60px] ",
            {
              "pl-12 pr-5": leftIcon,
              "pr-10": rightIcon,
              "text-inherit": isActive,
            },
            className
          )}
          rows="10"
          onFocus={() => setInputFocus(true)}
          onBlur={handleBlur}
        />
      ) : (
        <input
          type={type}
          id={id}
          ref={ref}
          value={value}
          disabled={isDisabled}
          autoComplete={"on"}
          onBlur={() => setInputFocus(false)}
          {...rest}
          className={classNames(
            "input-field ",
            {
              "pl-12 pr-10": leftIcon,
              "pr-3": rightIcon,
              "text-inherit": isActive,
              "text-disabled": isDisabled,
            },
            className
          )}
          onFocus={() => setInputFocus(true)}
        />
      );

    return (
      <div className={classNames("input-container", containerClassName)}>
        <label
          className={classNames(labelClassName, "input-label", {
            "input-label__focused": isActive,
            "pl-8": !isActive && leftIcon,
            "text-red": error,
          })}
        >
          {labelName}
          {required && <span className="text-red-900 text-sm ml-[2px]">*</span>}
        </label>
        <div className="w-full flex flex-col gap-1">
          <div
            className={classNames(
              "input-relative relative mt-2 ",
              { "bg-main-grey": isDisabled },
              inputRelativeClassName,
              {
                "border-red": error,
              }
            )}
          >
            {leftIcon && (
              <span className="input-icon_absolute input-icon_absolute--left">
                {leftIcon}
              </span>
            )}
            {renderInput()}
            {rightIcon && (
              <span className="input-icon_absolute input-icon_absolute--right">
                {rightIcon}
              </span>
            )}
          </div>

          {error && <ErrorInput message={error.message} />}
        </div>
      </div>
    );
  }
);

export default Input;
