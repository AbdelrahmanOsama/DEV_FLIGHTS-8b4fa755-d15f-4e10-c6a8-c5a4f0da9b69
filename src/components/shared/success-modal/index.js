import * as React from "react";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";

import logo from "assets/images/Logo-without-title.png";

import "./styles.scss";

const SuccessModal = ({ open }) => {
  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      open={open}
      className="flex items-center justify-center bg-white bg-opacity-50"
    >
      <Fade in={open}>
        <img src={logo} className="loader" />
      </Fade>
    </Modal>
  );
};

export default SuccessModal;
