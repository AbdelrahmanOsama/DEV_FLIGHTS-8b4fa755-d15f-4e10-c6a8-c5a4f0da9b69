import React, { useEffect } from "react";
import classNames from "classnames";
import Select, { components } from "react-select";
import ErrorInput from "../ErrorInput/ErrorInput";
import { CircularProgress } from "@mui/material";
import { useTranslation } from "react-i18next";

import "./selectOptions.scss";

const SelectOptions = React.forwardRef(
  (
    {
      name = "",
      placeholder = "",
      labelName = "",
      contentContainerClassName = "",
      containerClassName = "",
      inputRelativeClassName = "",
      labelClassName = "",
      customOption = null,
      avatar = false,
      options = [],
      required = false,
      value,
      onChange,
      loading,
      defaultValue = null,
      error,
      disabled,
      direction,
      ...rest
    },
    ref
  ) => {
    const [isActive, setActive] = React.useState(!!value?.value);
    const { t } = useTranslation();
    useEffect(() => {
      if (value?.value) {
        setActive(true);
      } else {
        setActive(false);
      }
    }, [value]);

    const setInputFocus = (status) =>
      value ? setActive(true) : setActive(status);

    // default option DOM
    const defaultCustomOption = (props) => (
      <components.Option
        className={`autoHeight ${
          direction === "rtl" ? "flex-row-reverse" : ""
        }`}
        {...props}
      >
        <span className="option-name">{props?.label}</span>
      </components.Option>
    );

    return (
      <div className={contentContainerClassName}>
        <label
          className={classNames("input-label", labelClassName, {
            "input-label__focused": isActive,
            "text-red": isActive && error,
            "text-trans": value?.value,
          })}
        >
          {labelName}
          {required && <span className="text-red-900 text-sm ml-[2px]">*</span>}
        </label>
        <div className={classNames("input-relative mt-3", containerClassName)}>
          {loading ? (
            <div className="flex justify-center place-items-center h-[50px]">
              <CircularProgress size={30} />
            </div>
          ) : (
            <div
              className={classNames("relative select", inputRelativeClassName, {
                "bg-main-grey": disabled,
              })}
            >
              <Select
                className="select-overlay"
                classNamePrefix="select"
                styles={{
                  indicatorSeparator: () => ({ display: "none" }),
                  control: (base) => ({
                    ...base,
                    border: "none",
                    borderRadius: "4px",
                    minHeight: "44px",
                    fontSize: "14px",
                    paddingLeft: "6px",
                  }),
                  menuPortal: (base) => ({ ...base, zIndex: 9999 }),
                  option: (base, state) => ({
                    ...base,
                    backgroundColor: state.isSelected && "#F4F4F4",
                    borderBottom: "1px solid #f4f4f4",
                    height: 66,
                    display: "flex",
                    alignItems: "center",
                    color: "#333",
                    "&:hover": {
                      backgroundColor: "#F4F4F4",
                    },
                  }),
                }}
                menuPortalTarget={document.body}
                placeholder={placeholder}
                components={{ Option: customOption || defaultCustomOption }} // if null use defaultCustomOption
                name={name}
                options={options}
                value={value || defaultValue}
                indicator
                onChange={onChange}
                ref={ref}
                isDisabled={disabled}
                noOptionsMessage={() => t("no_options")}
                {...rest}
                onFocus={() => setInputFocus(true)}
                onBlur={() => setInputFocus(false)}
              />
              <fieldset
                aria-hidden="true"
                className={classNames("", {
                  "border-red": error,
                  "border-trans": value?.value,
                })}
              >
                <legend
                  className={classNames({
                    "legend--focused": isActive,
                    "border-red": error,
                    "border-trans": value?.value,
                  })}
                >
                  <span>{placeholder}</span>
                </legend>
              </fieldset>
            </div>
          )}
        </div>
        {error && value == undefined && (
          <ErrorInput message={error?.value?.message} />
        )}
      </div>
    );
  }
);

export default SelectOptions;
