import React, { memo, useEffect } from "react";
import classNames from "classnames";
import isEqual from "lodash/isEqual";
import { useTranslation } from "react-i18next";
import CircularLoader from "components/shared/circular-loader";

// Utils
import useClickOutside from "utils/clickoutside";

// images
import Search from "assets/icons/search.svg";
import ArrowDown from "assets/images/arrow-down.svg";
import ArrowUp from "assets/images/arrow-up.svg";

// styles
import "./selectOptions.scss";

const FilterOptions = ({
  name,
  options = [],
  filterIcon,
  label,
  isMulti,
  onApplyFilters,
  filters,
  filterCallback = undefined,
  ...rest
}) => {
  const [appendOptions, setAppendOptions] = React.useState(false);
  const [filterOptions, setFilterOptions] = React.useState(options);
  const [searchText, setChangeSearchText] = React.useState("");
  const [isLoading, setLoading] = React.useState(false);
  const [selectedOption, setSelectedOption] = React.useState(
    isMulti ? [] : filters?.[name]?.value
  );
  const filterRef = React.createRef();
  const { t } = useTranslation();

  useEffect(() => {
    if (
      (filters && filters === null) ||
      (filters && Object?.values(filters)?.filter((f) => f).length === 0)
    ) {
      setSelectedOption(isMulti ? [] : null);
    }

    // sync multi selectedOption with filters
    if (isMulti && filters && filters[name]) {
      setSelectedOption(filters[name].map(({ value }) => value));
    }

    // sync single selectedOption with filters
    if (!isMulti && filters && filters[name]) {
      setSelectedOption(filters[name]?.value);
    }
  }, [filters, isMulti, name]);

  // debounce search
  useEffect(() => {
    let getData;
    if (filterCallback && searchText) {
      setLoading(true);
      getData = setTimeout(async () => {
        const data = await filterCallback(searchText);
        setFilterOptions(data);
        setLoading(false);
      }, 2000);
    }

    return () => clearTimeout(getData);
  }, [searchText, filterCallback]);

  // custom hook to close when click outside menu
  useClickOutside(filterRef, () => setAppendOptions(false));

  // on change search input text handler
  const searchOptions = (event) => {
    // prevent default behavior
    event.preventDefault();

    // set search term
    setChangeSearchText(event.target.value);

    // if outside filter handler does not exist, filter internally by name
    if (!filterCallback) {
      setFilterOptions(
        options?.filter((option) =>
          option.label
            .toLowerCase()
            .startsWith(event.target.value.toLowerCase())
        )
      );
    }
  };

  // filter collection value format
  const setFilterValue = (option) => {
    if (isMulti) {
      return filters && filters[name]
        ? [
            ...filters[name],
            {
              ...option,
              filterIcon: filterIcon,
            },
          ]
        : [
            {
              ...option,
              filterIcon: filterIcon,
            },
          ];
    }

    return {
      ...option,
      filterIcon: filterIcon,
    };
  };

  // get Options list
  const getOptionsList = () => {
    // if user is searching, show filtered results
    if (searchText) return filterOptions;

    // otherwise, return total options
    return options;
  };

  return (
    <div className="relative" ref={filterRef}>
      <div
        className="header-filter flex items-center"
        onClick={() => setAppendOptions(!appendOptions)}
      >
        {filterIcon}
        <span className="filter-style__label">{label}</span>
        {appendOptions ? (
          <img src={ArrowUp} alt="" />
        ) : (
          <img src={ArrowDown} alt="" />
        )}
      </div>
      {appendOptions && (
        <div className="floating-filter-options">
          <div className="floating-filter-option floating-filter-option--search flex items-center justify-between gap-2">
            {isLoading ? <CircularLoader /> : <img src={Search} alt="search" />}
            <input
              type="text"
              value={searchText}
              onChange={searchOptions}
              autoFocus
              className="filter-style__label label-input"
              placeholder={t("search_and_select")}
            />
          </div>
          {getOptionsList()?.map((option, index) => (
            <div
              key={`${option.label}-${index}`}
              className={classNames(
                "floating-filter-option flex items-center gap-2",
                {
                  "bg-selected": Array.isArray(selectedOption)
                    ? selectedOption.includes(option.value)
                    : selectedOption === option.value,
                }
              )}
              onClick={() => {
                // if same filter clicked, do nothing
                if (
                  (Array.isArray(selectedOption) &&
                    selectedOption.includes(option.value)) ||
                  selectedOption === option.value
                ) {
                  return null;
                }

                // close dropdown
                setAppendOptions(false);
                // remove search text
                setChangeSearchText("");
                // update selected list items
                setSelectedOption(
                  isMulti ? [...selectedOption, option.value] : option.value
                );
                // send collected/selected filters to be applied
                onApplyFilters({
                  [name]: setFilterValue(option),
                });
              }}
              {...rest}
            >
              {option.icon && typeof option.icon === "string" ? (
                <img src={option.icon} alt="filter icon" />
              ) : (
                option.icon
              )}
              <span className="filter-style__label" title={option.label}>
                {option.label}
              </span>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default memo(FilterOptions, (prevProps, nextProps) => {
  isEqual(prevProps.options, nextProps.options);
});
