import React from "react";

function GrayBox({ children, className }) {
  return (
    <div
      className={`py-6 px-4 sm:p-6 md:p-8 bg-main-grey sm:rounded-lg w-full flex flex-col  gap-10  ${className}`}
    >
      {children}
    </div>
  );
}

export default GrayBox;
