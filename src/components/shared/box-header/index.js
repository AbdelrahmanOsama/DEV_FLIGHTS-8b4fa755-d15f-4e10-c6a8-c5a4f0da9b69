import React from "react";
import ArrowBackRoundedIcon from "@mui/icons-material/ArrowBackRounded";
import classNames from "classnames";

import "./box-header.scss";
import { useTranslation } from "react-i18next";

const BoxHeader = ({
  title,
  icon,
  divider,
  allButton,
  containerClassName,
  onClick = () => {},
}) => {
  const { t } = useTranslation();
  return (
    <>
      <div
        className={classNames(
          "flex justify-between w-full",
          containerClassName
        )}
      >
        <div className="flex items-center justify-center gap-4">
          <img src={icon} className="h-8 w-8 header_icon" />
          <h2 className="font-bold sm:text-2xl text-xl text-primary-dark ">
            {title}
          </h2>
        </div>

        {allButton && (
          <button
            onClick={onClick}
            className="px-5 bg-primary-blue flex items-center justify-center gap-2 rounded-md h-9 transition duration-300 ease-in-out hover:scale-110 "
          >
            <p className="text-white text-sm ">{t("all")}</p>
            <ArrowBackRoundedIcon
              fontSize="small"
              className="text-white  ltr:rotate-180 "
            />
          </button>
        )}
      </div>
      {divider && <div className="w-full h-[1.5px] bg-primary-dark/10" />}
    </>
  );
};

export default BoxHeader;
