import * as React from "react";
import { useTranslation } from "react-i18next";

import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import Button from "../Button";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";

const BlockModal = ({
  title,
  icon,
  description,
  open,
  extraDesc,
  onClose,
  actionTitle,
  handleAction,
  loading
}) => {
  const { t } = useTranslation();
  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      open={open}
      className="flex items-center justify-center"
    >
      <Fade in={open}>
        <div className="relative bg-white flex  flex-col items-center gap-8 py-10 px-8 sm:px-12 md:px-20 mx-6 rounded-lg max-w-4xl ">
          {icon}

          <div className="flex flex-col items-center gap-4">
            <h2 className=" text-xl md:text-2xl text-center font-bold text-primary-blue  ">
              {title}
            </h2>
            <p className="md:text-base text-primary-dark text-center">
              {description}
              {extraDesc && (
                <span className="font-medium">{`" ${extraDesc} "`}</span>
              )}
            </p>
          </div>

          <button
            onClick={onClose}
            className=" absolute rtl:left-3 ltr:right-3 top-3 "
          >
            <CloseRoundedIcon
              sx={{ fontSize: "32px" }}
              className="m-2 text-primary-dark/60 "
            />
          </button>
          <div className="flex gap-6">
            <Button
              type="submit"
              classContainer="h-[40px] w-[8rem] px-0 flex-row-reverse"
              labelClassContainer="text-white xl:text-base text-sm font-medium"
              label={actionTitle}
              isLoading={loading}
              handleAction={handleAction}
            />

            <Button
              type="button"
              classContainer="h-[40px] w-[8rem] px-0 flex-row-reverse !bg-[#C61010]"
              labelClassContainer="text-[#fff] xl:text-base text-sm font-medium"
              label={t("cancel")}
              handleAction={onClose}
            />
          </div>
        </div>
      </Fade>
    </Modal>
  );
};

export default BlockModal;
