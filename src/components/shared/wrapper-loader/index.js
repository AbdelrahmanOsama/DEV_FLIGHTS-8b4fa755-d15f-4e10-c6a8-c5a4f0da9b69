import React from "react";
import { useTranslation } from "react-i18next";

import CircularLoader from "./loader";

const WrapperLoader = ({ onReload, error, loading, children }) => {
  const { t } = useTranslation();

  return loading || error ? (
    <div className="h-full w-full flex flex-col items-center justify-center">
      {loading ? (
        <CircularLoader />
      ) : (
        <div className="text-center">
          <h1 className="mt-4 text-3xl font-bold tracking-tight text-primary-dark sm:text-5xl">
            {t("oops")}
          </h1>
          <p className="mt-6 text-base leading-7 text-primary-dark/60">
            {t("sorrySomethingWenWrong")}
          </p>
          <div className="mt-10 flex items-center justify-center gap-x-6">
            <button
              type="button"
              onClick={onReload}
              className="rounded-md bg-primary-blue px-6 py-3 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
            >
              <button>{t("tryAgain")}</button>
            </button>
          </div>
        </div>
      )}
    </div>
  ) : (
    children
  );
};

export default WrapperLoader;
