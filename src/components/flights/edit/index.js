import React from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import moment from "moment";

// Components
import validation from "./validations";
import Button from "components/shared/Button";
import Input from "components/shared/Input/Input";
import WhiteBox from "components/shared/white-box";

// API
import {
  checkFlightCode,
  updateFlightWithoutPhoto,
  updateFlightWithPhoto,
  getFlightById,
} from "api/flights/flights";

// Utils
import useDocumentTitle from "utils/dynamic-title";
import useToast from "utils/toast";
import SingleInputFile from "components/shared/single-input-file";
import { axiosInstance } from "api/api";
import { Base_URL } from "constants";

const EditFlight = () => {
  const [attachments, setAttachments] = React.useState([]);
  const [isVerifying, setIsVerifying] = React.useState(false);
  const [flightDetails, setFlightDetails] = React.useState({});
  const { id } = useParams();
  const navigate = useNavigate();
  const { toast } = useToast();

  useDocumentTitle("Update Flight");
  const currentPhoto = `${Base_URL}/flights/${id}/photo`;

  const {
    register,
    handleSubmit,
    watch,
    setValue,
    control,
    clearErrors,
    setError,
    formState: { errors },
  } = useForm({
    mode: "onSubmit",
    resolver: yupResolver(validation),
    defaultValues: {
      code: "",
      capacity: "",
      departureDate: "",
      img: "",
    },
  });

  React.useEffect(() => {
    const getFlightDetails = async () => {
      try {
        const response = await getFlightById(id);
        setFlightDetails(response?.data);
        setValue("code", response?.data?.code);
        setValue("capacity", response?.data?.capacity);
        setValue("departureDate", response?.data?.departureDate);
        setValue("img", response?.data?.img);

        if (response?.data?.img) {
          const file = new File([response?.data?.img], response?.data?.img);
          setAttachments([file]);
        }
      } catch (error) {}
    };

    getFlightDetails();
  }, [id]);

  const checkCode = async () => {
    try {
      const response = await checkFlightCode(watch("code"));
      return response;
    } catch (error) {}
  };

  const isCodeVerified = React.useCallback(async () => {
    try {
      setIsVerifying(true);
      const response = await checkCode();
      setIsVerifying(false);
      if (response?.data?.status === "available") {
        clearErrors("code");
        return true;
      } else {
        setError("code", {
          message: "Code must be a unique value",
        });
        return false;
      }
    } catch (error) {
      setError("code", {
        type: "manual",
        message: "Check again later",
      });
      setIsVerifying(false);
      return false;
    }
  }, [checkCode, clearErrors, setError]);

  const updateFlightWithPhoto = async (payload) => {
    try {
      // Create a new FormData object
      const formData = new FormData();
      formData.append("photo", flightDetails?.img);

      // Append other payload data to the FormData object
      formData.append("code", payload.code);
      formData.append("capacity", payload.capacity);
      formData.append("departureDate", payload.departureDate);

      // Set the Content-Type to multipart/form-data
      axiosInstance.defaults.headers.common["Content-Type"] =
        "multipart/form-data";

      // Send the FormData object
      await updateFlightWithPhoto(formData);
    } catch (error) {}
  };

  const onSubmit = async ({ code, capacity, departureDate }) => {
    const codeValid = await isCodeVerified();
    if (!codeValid && code !== flightDetails?.code) {
      return; // Prevent form submission if code is not valid
    }

    const payload = {
      code,
      capacity,
      departureDate: moment(departureDate).format("YYYY-MM-DD"),
    };
    try {
      flightDetails?.img
        ? await updateFlightWithPhoto(payload)
        : await updateFlightWithoutPhoto(id, payload);
      navigate(`/home?page=1&size=10`, { replace: true });
    } catch (error) {}
  };

  return (
    <WhiteBox className={"visible_animation"}>
      <form
        autoComplete="false"
        className="w-full flex flex-col gap-8 h-full justify-between"
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className="grid content-center gap-8 grid-cols-1 xl:grid-cols-2 ">
          <Controller
            {...{
              control,
              name: "code",
              render: ({ field }) => (
                <Input
                  required
                  labelName="Code"
                  placeholder="Code"
                  type="text"
                  error={errors.code}
                  id="code"
                  disabled={isVerifying}
                  {...register("code", {
                    required: true,
                  })}
                  {...field}
                />
              ),
            }}
          />
          <Controller
            {...{
              control,
              name: "capacity",
              render: ({ field }) => (
                <Input
                  required
                  labelName="Capacity"
                  placeholder="Capacity"
                  type="number"
                  error={errors.capacity}
                  id="capacity"
                  {...register("capacity", {
                    required: true,
                  })}
                  {...field}
                />
              ),
            }}
          />
          <Controller
            {...{
              control,
              name: "departureDate",
              render: ({ field }) => (
                <Input
                  required
                  labelName="Departure Date"
                  placeholder="Departure Date"
                  type="date"
                  error={errors.departureDate}
                  id="departureDate"
                  {...register("departureDate", {
                    required: true,
                  })}
                  {...field}
                />
              ),
            }}
          />

          {flightDetails?.img && (
            <Controller
              {...{
                control,
                name: "img",
                render: ({ field }) => (
                  <SingleInputFile
                    label="Image"
                    value={attachments}
                    url={currentPhoto}
                    required
                    disabled
                    inProgress={() => {
                      toast("success", "Image Uploaded Successfully");
                    }}
                    allowedExtension={["jpeg, png, jpg"]}
                    handleDelete={() => {
                      setAttachments([]);
                    }}
                    onChange={(data) => {
                      setAttachments(data);
                    }}
                  />
                ),
              }}
            />
          )}
        </div>

        <div className="px-4 w-full sm:px-0 flex gap-4 w-fil mb-8 justify-end">
          <Button
            label={"Cancel"}
            outline
            classContainer={
              "w-[8rem] h-[40px] group hover:bg-primary-blue !border-[#005EB8] !border-1"
            }
            labelClassContainer="!text-[#005EB8] group-hover:!text-[#fff] xl:text-base text-sm font-medium"
            handleAction={() =>
              navigate(`/home?page=1&size=10`, { replace: true })
            }
          />
          <Button
            type="submit"
            label="Edit Flight"
            classContainer="w-[8rem] h-[40px] px-0 flex-row-reverse"
            labelClassContainer="text-white xl:text-base text-sm font-medium"
          />
        </div>
      </form>
    </WhiteBox>
  );
};

export default EditFlight;
