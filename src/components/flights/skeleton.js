import React from "react";
import Skeleton from "@mui/material/Skeleton";

export default function CustomSkeleton() {
  return (
    <div className="flex justify-between bg-main-grey rounded-lg p-6 w-full">
      <p className="flex flex-col font-medium text-xl text-primary-dark place-content-center mb-0 flex-1">
        <span className="text-primary-blue text-xs font-medium text-exceed-limit">
          <Skeleton
            sx={{ bgcolor: "rgba(0, 94, 184, .2)" }}
            animation="wave"
            variant="rounded"
            width={80}
            height={30}
          />
        </span>
      </p>
      <p className="font-normal text-xs text-primary-dark place-self-center items-center hidden sm:flex gap-4 flex-1">
        <span>
          {" "}
          <Skeleton
            sx={{ bgcolor: "rgba(0, 94, 184, .2)" }}
            animation="wave"
            variant="rounded"
            width={80}
            height={30}
          />
        </span>
      </p>
      <p className="font-normal text-xs text-primary-dark place-self-center items-center hidden sm:flex  gap-4 flex-1">
        <span>
          {" "}
          <Skeleton
            sx={{ bgcolor: "rgba(0, 94, 184, .2)" }}
            animation="wave"
            variant="rounded"
            width={80}
            height={30}
          />
        </span>
      </p>
      <p className="font-normal text-xs text-primary-dark place-self-center items-center hidden sm:flex  gap-4 flex-1">
        <span>
          {" "}
          <Skeleton
            sx={{ bgcolor: "rgba(0, 94, 184, .2)" }}
            animation="wave"
            variant="rounded"
            width={80}
            height={30}
          />
        </span>
      </p>
      <p className="font-normal text-xs text-primary-dark place-self-center items-center hidden sm:flex  gap-4 flex-1">
        <span>
          {" "}
          <Skeleton
            sx={{ bgcolor: "rgba(0, 94, 184, .2)" }}
            animation="wave"
            variant="rounded"
            width={80}
            height={30}
          />
        </span>
      </p>
      <div className="font-medium text-xl text-primary-dark flex-1">
        <Skeleton
          sx={{ bgcolor: "rgba(0, 94, 184, .2)" }}
          animation="wave"
          variant="rounded"
          width={80}
          height={30}
        />
      </div>

    </div>
  );
}
