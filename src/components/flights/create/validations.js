import * as yup from "yup";

export default yup.object().shape({
  code: yup
    .string()
    .trim("Invalid White Spaces")
    .strict(false)
    .matches(
      /^[a-zA-Z]{6}$/,
      "Code must be exactly 6 characters long and contain only uppercase or lowercase letters"
    )
    .required("Invalid Field"),
  capacity: yup
    .number()
    .typeError("Invalid Number Field") // Ensure it provides a clear error for non-numeric inputs
    .integer("Invalid Number Field") // Ensure the number is an integer
    .min(1, "Capacity must be at least 1")
    .max(200, "Capacity must be at most 200")
    .required("Invalid Field"),
  departureDate: yup.string().required("Invalid Field"),
});
