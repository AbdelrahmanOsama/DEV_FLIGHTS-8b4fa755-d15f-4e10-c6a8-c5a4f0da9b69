import React from "react";
import { useNavigate } from "react-router-dom";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import moment from "moment";

// Components
import validation from "./validations";
import Button from "components/shared/Button";
import Input from "components/shared/Input/Input";
import WhiteBox from "components/shared/white-box";

// API
import {
  checkFlightCode,
  createFlightWithPhoto,
  createFlightWithoutPhoto,
} from "api/flights/flights";

// Utils
import useDocumentTitle from "utils/dynamic-title";
import useToast from "utils/toast";
import SingleInputFile from "components/shared/single-input-file";
import { isEmpty } from "lodash";
import { axiosInstance } from "api/api";

const CreateFlight = () => {
  const [attachments, setAttachments] = React.useState([]);
  const [isVerifying, setIsVerifying] = React.useState(false);

  const navigate = useNavigate();
  const { toast } = useToast();

  useDocumentTitle("Create New Flight");

  const {
    register,
    handleSubmit,
    watch,
    control,
    clearErrors,
    setError,
    formState: { errors },
  } = useForm({
    mode: "onSubmit",
    resolver: yupResolver(validation),
    defaultValues: {
      code: "",
      capacity: "",
      departureDate: "",
      photo: "",
    },
  });

  const checkCode = async () => {
    try {
      const response = await checkFlightCode(watch("code"));
      return response;
    } catch (error) {}
  };

  const isCodeVerified = React.useCallback(async () => {
    try {
      setIsVerifying(true);
      const response = await checkCode();
      setIsVerifying(false);
      if (response?.data?.status === "available") {
        clearErrors("code");
        return true;
      } else {
        setError("code", {
          message: "Code must be a unique value",
        });
        return false;
      }
    } catch (error) {
      setError("code", {
        type: "manual",
        message: "Check again later",
      });
      setIsVerifying(false);
      return false;
    }
  }, [clearErrors, setError, watch]);

  const sendRequestWithPhoto = async (payload) => {
    try {
      // Create a new FormData object
      const formData = new FormData();

      // Append the photo file(s) to the FormData object
      if (Array.isArray(attachments)) {
        attachments.forEach((file, index) => {
          formData.append(`photo`, file);
        });
      } else {
        formData.append("photo", attachments);
      }

      // Append other payload data to the FormData object
      formData.append("code", payload.code);
      formData.append("capacity", payload.capacity);
      formData.append("departureDate", payload.departureDate);

      // Set the Content-Type to multipart/form-data
      axiosInstance.defaults.headers.common["Content-Type"] =
        "multipart/form-data";

      // Send the FormData object using Axios
      const response = await createFlightWithPhoto(formData);
      return response;
    } catch (error) {
      // Handle error
      console.error("Error sending request with photo:", error);
    }
  };

  const onSubmit = async ({ code, capacity, departureDate }) => {
    const codeValid = await isCodeVerified();
    if (!codeValid) {
      return; // Prevent form submission if code is not valid
    }

    const payload = {
      code,
      capacity,
      departureDate: moment(departureDate).format("YYYY-MM-DD"),
    };
    try {
      const response =
        attachments.length > 0
          ? await sendRequestWithPhoto(payload)
          : await createFlightWithoutPhoto(payload);

      navigate(`/flight/success`, {
        state: {
          title: "Flight Created Successfully",
          reference: response?.data?.code,
          redirectUrl: `/flight/${response?.data?.id}`,
        },
        replace: true,
      });
    } catch (error) {}
  };

  return (
    <WhiteBox className={"visible_animation"}>
      <form
        autoComplete="false"
        className="w-full flex flex-col gap-8 h-full justify-between"
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className="grid content-center gap-8 grid-cols-1 xl:grid-cols-2 ">
          <Controller
            {...{
              control,
              name: "code",
              render: ({ field }) => (
                <Input
                  required
                  labelName="Code"
                  placeholder="Code"
                  type="text"
                  error={errors.code}
                  id="code"
                  disabled={isVerifying}
                  {...register("code", {
                    required: true,
                  })}
                  {...field}
                />
              ),
            }}
          />
          <Controller
            {...{
              control,
              name: "capacity",
              render: ({ field }) => (
                <Input
                  required
                  labelName="Capacity"
                  placeholder="Capacity"
                  type="number"
                  error={errors.capacity}
                  id="capacity"
                  {...register("capacity", {
                    required: true,
                  })}
                  {...field}
                />
              ),
            }}
          />
          <Controller
            {...{
              control,
              name: "departureDate",
              render: ({ field }) => (
                <Input
                  required
                  labelName="Departure Date"
                  placeholder="Departure Date"
                  type="date"
                  error={errors.departureDate}
                  id="departureDate"
                  {...register("departureDate", {
                    required: true,
                  })}
                  {...field}
                />
              ),
            }}
          />

          <Controller
            {...{
              control,
              name: "img",
              render: ({ field }) => (
                <SingleInputFile
                  label="Image"
                  value={attachments}
                  required
                  inProgress={() => {
                    toast("success", "Image Uploaded Successfully");
                  }}
                  allowedExtension={["jpeg, png, jpg"]}
                  handleDelete={() => {
                    setAttachments([]);
                  }}
                  onChange={(data) => {
                    setAttachments(data);
                  }}
                />
              ),
            }}
          />
        </div>

        <div className="px-4 w-full sm:px-0 flex gap-4 w-fil mb-8 justify-end">
          <Button
            label={"Cancel"}
            outline
            classContainer={
              "w-[8rem] h-[40px] group hover:bg-primary-blue !border-[#005EB8] !border-1"
            }
            labelClassContainer="!text-[#005EB8] group-hover:!text-[#fff] xl:text-base text-sm font-medium"
            handleAction={() =>
              navigate(`/home?page=1&size=10`, { replace: true })
            }
          />
          <Button
            type="submit"
            label="Create "
            classContainer="w-[8rem] h-[40px] px-0 flex-row-reverse"
            labelClassContainer="text-white xl:text-base text-sm font-medium"
          />
        </div>
      </form>
    </WhiteBox>
  );
};

export default CreateFlight;
