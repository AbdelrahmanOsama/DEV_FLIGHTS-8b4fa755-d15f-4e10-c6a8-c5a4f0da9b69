import React from "react";
import WhiteBox from "components/shared/white-box";
import { useNavigate, useLocation, Navigate } from "react-router-dom";

import successIcon from "assets/images/success.svg";

const SuccessPage = () => {
  const navigate = useNavigate();
  const location = useLocation();

  const data = location?.state;

  const onClick = () => {
    navigate(data?.redirectUrl, {
      replace: true,
    });
  };

  const goToHome = () => {
    navigate("/home?page=1&size=10", {
      replace: true,
    });
  };

  if (!data) return <Navigate to="/home?page=1&size=10" replace />;
  return (
    <WhiteBox>
      <div className="flex flex-col items-center justify-center h-full w-full">
        <img src={successIcon} className="h-28 w-28" />

        <h2 className=" text-xl font-medium text-primary-dark mt-10 text-center ">
          {data?.title}
        </h2>
        <h2 className=" font-medium text-primary-dark mt-5 ">
          {`Code
            :  ${data?.reference}`}
        </h2>

        <p className=" text-primary-dark mt-5 text-center">
          {data?.descriptionEn}
        </p>
        <div className="flex items-center gap-3">
          <button
            onClick={onClick}
            className=" flex items-center justify-center w-44 bg-primary-blue text-white font-medium rounded-md px-10 py-3 my-10"
          >
            Flight Details
          </button>

          <button
            onClick={() => goToHome()}
            className=" flex items-center justify-center w-44 bg-primary-dark/50 text-white font-medium rounded-md px-4 sm:px-10 py-3  my-10"
          >
            Flights
          </button>
        </div>
      </div>
    </WhiteBox>
  );
};

export default SuccessPage;
