import React, { useCallback, useEffect, useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import moment from "moment";
import useToast from "utils/toast";

// component
import Button from "components/shared/Button";
import CustomSkeleton from "../skeleton";
import CustomizedTables from "./table";
import CustomPagination from "components/shared/Paginations";
import EmptyContent from "components/shared/empty-content";
import RequestStatus from "components/shared/request-status";
import CustomizedDialogs from "components/shared/Modal/Modal";

// API
import {
  GetFlightList,
  checkFlightsStatus,
  deleteFlight,
} from "api/flights/flights";

// Assets + Icons
import EmptyData from "assets/images/empty-data.svg";
import AddIcon from "@mui/icons-material/Add";
import CalendarTodayOutlinedIcon from "@mui/icons-material/CalendarTodayOutlined";
import PhotoIcon from "@mui/icons-material/Photo";
import BorderColorIcon from "@mui/icons-material/BorderColor";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import { Base_URL } from "constants";
import Input from "components/shared/Input/Input";

export default function Requests() {
  const [isFetching, setIsFetching] = useState(true);
  const [searchTerm, setSearchTerm] = useState("");
  const [reqData, setReqData] = useState([]);
  const [selectedRow, setSelectedRow] = useState(null);
  const [show, setShow] = React.useState(false);
  const [showFlight, setShowFlight] = React.useState(false);
  const [isDeleteLoading, setIsDeleteLoading] = useState(false);
  const [previewUrl, setPreviewUrl] = React.useState(null);
  const { toast } = useToast();
  const location = useLocation();
  const query = new URLSearchParams(location.search);

  const previewPhoto = (id) => {
    try {
      setShow(true);
      const photoURL = `${Base_URL}/flights//${id}/photo`;
      setPreviewUrl(photoURL);
    } catch (error) {}
  };

  const editFlight = (id) => {
    navigate(`/flight/${id}`, { replace: true });
  };

  const openDeleteDialog = (row) => {
    setSelectedRow(row);
    setShowFlight(true);
  };

  const deleteFlightAction = async () => {
    const id = selectedRow?.id;
    try {
      setIsDeleteLoading(true);
      await deleteFlight(id);
      setTimeout(() => {
        fetchRequests();
        setShowFlight(false);
        toast("success", "Flight deleted successfully");

        setIsDeleteLoading(false);
      }, 500);
    } catch (error) {
      console.log("Error deleting flight", error);
      toast("error", "Error deleting flight");
      setIsDeleteLoading(false);
    }
  };

  const [columns] = useState([
    { name: "Item", key: "item", link: true, linkPrefix: "/flight" },
    { name: "Code", key: "code", link: true, linkPrefix: "/flight" },
    { name: "Capacity", key: "capacity", link: true, linkPrefix: "/flight" },
    {
      name: "Departure Date",
      key: "departureDate",
      link: true,
      linkPrefix: "/flight",
      render: (row) => (
        <>
          <CalendarTodayOutlinedIcon
            sx={{ fontSize: "18px", color: "#40C1AC" }}
          />
          <span> {row?.departureDate}</span>
        </>
      ),
    },
    {
      name: "Status",
      key: "status",
      link: true,
      linkPrefix: "/flight",
      render: (row) => (
        <RequestStatus
          classContainer="lg:justify-start justify-end"
          className="py-1"
          statusID={row?.status}
          statusName={row?.status}
        />
      ),
    },
    {
      name: "Photo",
      key: "photo",
      render: (row) => (
        <PhotoIcon
          sx={{ color: row?.img ? "#40C1AC" : "#8FA0B4" }}
          onClick={() => previewPhoto(row?.id)}
        />
      ),
    },
    {
      name: "",
      key: "action",
      render: (row) => (
        <div className="flex gap-5">
          <BorderColorIcon
            sx={{ color: "#005EB8" }}
            onClick={() => editFlight(row?.id)}
          />
          <DeleteForeverIcon
            sx={{ color: "#C61010" }}
            onClick={() => openDeleteDialog(row)}
          />
        </div>
      ),
    },
  ]);
  const [currentPage, setCurrentPage] = useState(
    parseInt(query.get("page")) || 1
  );
  const itemsPerPage = 10;
  const navigate = useNavigate();

  const changePage = (e, page) => {
    navigate(`/home?page=${page}`, { replace: true });
  };

  // if the params change, update the currentPage
  useEffect(() => {
    setCurrentPage(parseInt(query.get("page")) || 1);
  }, [query]);

  const renderCellContent = (row, column) => {
    if (column.render) {
      return column.render(row);
    }
    return (
      <div className="flex text-xs md:text-sm lg:text-base gap-2 pb-2">
        {row[column.key]}
      </div>
    );
  };

  const fetchRequests = useCallback(async () => {
    setIsFetching(true);
    try {
      const payload = {
        page: currentPage,
        size: itemsPerPage,
        code: searchTerm,
      };

      const requests = await GetFlightList(payload);
      setReqData({
        data: requests?.data?.resources.map((val, key) => ({
          item: key + 1,
          id: val?.id,
          code: val?.code,
          departureDate: moment(val?.departureDate).format("DD/MM/YYYY"),
          status: val?.status,
          capacity: val?.capacity,
          img: val?.img,
        })),
        total: requests?.data?.total,
      });
      setTimeout(() => {
        setIsFetching(false);
      }, 200);
    } catch (error) {
      setReqData([]);
      setTimeout(() => {
        setIsFetching(false);
      }, 500);
    }
  }, [currentPage, searchTerm]);

  const pollFlightStatus = useCallback(() => {
    const pendingFlights = reqData.data.filter(
      (flight) => flight.status === "processing"
    );
    if (pendingFlights.length > 0) {
      const flightIds = pendingFlights.map((flight) => flight.id);
      checkFlightsStatus(flightIds).then((updatedFlights) => {
        // Update the status and photo of the flights
        const updatedData = reqData.data.map((flight) => {
          const updatedFlight = updatedFlights?.resources?.find(
            (updatedFlight) => updatedFlight.id === flight.id
          );
          if (updatedFlight) {
            return {
              ...flight,
              status: updatedFlight.status,
              img: updatedFlight.img,
            };
          }
          return flight;
        });
        setReqData({ ...reqData, data: updatedData });
      });
    }
  }, [reqData.data]);

  useEffect(() => {
    const intervalId = setInterval(pollFlightStatus, 15000); // Poll every 30 seconds
    return () => clearInterval(intervalId); // Cleanup interval on component unmount
  }, [pollFlightStatus]);

  useEffect(() => {
    fetchRequests();
  }, [currentPage, fetchRequests]);

  return (
    <>
      <div className="mb-6 w-full flex justify-end">
        <Button
          label="Create Flight"
          classContainer="h-[40px] px-0 flex-row-reverse"
          labelClassContainer="text-white xl:text-base text-sm font-medium"
          handleAction={() => navigate(`/flight`, { replace: true })}
          iconComponent={<AddIcon sx={{ color: "#fff" }} />}
        />
      </div>

      <div className="w-full">
        <Input
          placeholder="Search by code"
          type="text"
          className="h-[55px] placeholder-primary-grey bg-white w-full"
          inputRelativeClassName="bg-white border-2 border-primary-dark/10 rounded-md "
          id="health"
          value={searchTerm}
          onChange={(e) => {
            setSearchTerm(e.target.value);
          }}
          onBlur={(e) => {
            setSearchTerm(e.target.value);
          }}
          onKeyDown={(e) => e.key === "Enter" && setSearchTerm(e.target.value)}
          leftIcon={
            <div>
              <i className="fa-solid fa-magnifying-glass absolute text-[#8FA0B4]"></i>
            </div>
          }
        />
      </div>

      {!isFetching ? (
        <>
          {reqData?.data?.length > 0 ? (
            <>
              <div className="w-full flex flex-col gap-5 transition duration-300 ease-in-out">
                <CustomizedTables
                  columns={columns}
                  rows={reqData?.data}
                  renderCellContent={renderCellContent}
                  editFlight={editFlight}
                  deleteDialog={openDeleteDialog}
                />
                <CustomPagination
                  itemsPerPage={itemsPerPage}
                  currentPage={currentPage}
                  total={reqData?.total}
                  changePage={changePage}
                />
              </div>
            </>
          ) : (
            <EmptyContent
              title={"No Requests Available"}
              description={"Please Create a Fight Request to Showed Here"}
              icon={EmptyData}
            />
          )}
        </>
      ) : (
        <>
          <CustomSkeleton />
          <CustomSkeleton />
          <CustomSkeleton />
          <CustomSkeleton />
          <CustomSkeleton />
          <CustomSkeleton />
        </>
      )}

      {/* Preview Image */}
      <CustomizedDialogs
        show={show}
        handleClose={() => setShow(false)}
        title="Image Preview"
      >
        <div className="flex flex-col items-center gap-4">
          <img
            src={previewUrl}
            alt="preview"
            className="w-full object-cover rounded-lg"
          />
          <button
            className="bg-primary-blue text-white px-4 py-2 rounded-lg"
            onClick={() => setShow(false)}
          >
            Close
          </button>
        </div>
      </CustomizedDialogs>

      {/* Delete Flight */}
      <CustomizedDialogs
        show={showFlight}
        handleClose={() => setShowFlight(false)}
        title="Delete Flight Confirmation"
      >
        <div className="flex flex-col items-center gap-4">
          <h3 className="text-lg text-primary-dark">
            Are you sure you want to delete this flight?
          </h3>
          <p className="text-sm text-primary-blue mb-5">
            Code:{" "}
            <span className="font-semibold underline">{selectedRow?.code}</span>
          </p>
          <div className="flex gap-7">
            <Button
              label={"Cancel"}
              outline
              classContainer={
                "w-[8rem] h-[40px] group hover:bg-primary-blue !border-[#005EB8] !border-1"
              }
              labelClassContainer="!text-[#005EB8] group-hover:!text-[#fff] xl:text-base text-sm font-medium"
              handleAction={() => setShowFlight(false)}
            />
            <Button
              type="submit"
              label="Confirm"
              classContainer="w-[8rem] h-[40px] px-0 flex-row-reverse"
              labelClassContainer="text-white xl:text-base text-sm font-medium"
              isLoading={isDeleteLoading}
              handleAction={deleteFlightAction}
            />
          </div>
        </div>
      </CustomizedDialogs>
    </>
  );
}
