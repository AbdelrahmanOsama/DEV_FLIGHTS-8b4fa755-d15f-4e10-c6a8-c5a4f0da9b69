import * as React from "react";
import { styled } from "@mui/material/styles";
import { Link } from "react-router-dom";

// Material UI
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import { Card, CardContent } from "@mui/material";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import useMediaQuery from "@mui/material/useMediaQuery";

// components
import RequestStatus from "components/shared/request-status";
import CustomizedDialogs from "components/shared/Modal/Modal";

// Assets
import PhotoIcon from "@mui/icons-material/Photo";
import BorderColorIcon from "@mui/icons-material/BorderColor";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import { Base_URL } from "constants";

const StyledTableCell = styled(TableCell)(() => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    color: "#002554",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(() => ({
  cursor: "pointer",
  "&:nth-of-type(odd)": {
    backgroundColor: "#F4F6F9",
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
    fontSize: 14,
  },
}));

export default function CustomizedTables({
  columns = [],
  rows = [],
  renderCellContent,
  editFlight,
  deleteDialog,
}) {
  const [show, setShow] = React.useState(false);
  const [previewUrl, setPreviewUrl] = React.useState(null);
  const isSmallScreen = useMediaQuery("(max-width:768px)");

  const previewPhoto = (id) => {
    try {
      setShow(true);
      const photoURL = `${Base_URL}/flights//${id}/photo`;
      setPreviewUrl(photoURL);
    } catch (error) {}
  };

  return (
    <>
      {!isSmallScreen ? (
        <TableContainer component={Paper} sx={{ boxShadow: "none" }}>
          <Table sx={{ minWidth: 700 }} aria-label="customized table">
            <TableHead>
              <TableRow>
                {columns.map(({ name }, index) => (
                  <StyledTableCell key={`${name}${index}`} align="left">
                    {name}
                  </StyledTableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <StyledTableRow key={row.id}>
                  {columns.map((column, colIndex) => (
                    <StyledTableCell
                      key={colIndex}
                      align="left"
                      component={column.link ? Link : "td"}
                      to={
                        column.link
                          ? `${column.linkPrefix}/${row?.id}`
                          : undefined
                      }
                      sx={column.sx}
                    >
                      {renderCellContent(row, column)}
                    </StyledTableCell>
                  ))}
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      ) : (
        rows.map((flight) => (
          <Card
            key={flight.id}
            style={{ marginBottom: "16px", backgroundColor: "#f0f8ff96" }}
          >
            <CardContent className="absolute right-10">
              <RequestStatus
                classContainer="lg:justify-start justify-end"
                className="py-1"
                statusID={flight?.status}
                statusName={flight?.status}
              />
            </CardContent>
            <CardContent className="flex flex-col gap-2">
              <h3>
                <span className="text-primary-blue">Code: </span> {flight.code}
              </h3>
              <p>
                <span className="text-primary-blue">Capacity: </span>{" "}
                {flight.capacity}
              </p>
              <p>
                <span className="text-primary-blue">Departure Date: </span>
                {flight.departureDate}
              </p>
              <div>
                <span className="text-primary-blue">Image: </span>{" "}
                <PhotoIcon
                  sx={{ color: flight?.img ? "#40C1AC" : "#8FA0B4" }}
                  onClick={() => previewPhoto(flight?.id)}
                />
              </div>
            </CardContent>
            <CardContent className="flex gap-8 justify-end !pb-4 !pt-0">
              <div className="font-semibold">
                Edit:{" "}
                <BorderColorIcon
                  sx={{ color: "#005EB8" }}
                  onClick={() => editFlight(flight?.id)}
                />
              </div>
              <div className="font-semibold">
                Delete:{" "}
                <DeleteForeverIcon
                  sx={{ color: "#C61010" }}
                  onClick={() => deleteDialog(flight)}
                />
              </div>
            </CardContent>
          </Card>
        ))
      )}

      <CustomizedDialogs
        show={show}
        handleClose={() => setShow(false)}
        title="Image Preview"
      >
        <div className="flex flex-col items-center gap-4">
          <img
            src={previewUrl}
            alt="preview"
            className="w-full object-cover rounded-lg"
          />
          <button
            className="bg-primary-blue text-white px-4 py-2 rounded-lg"
            onClick={() => setShow(false)}
          >
            Close
          </button>
        </div>
      </CustomizedDialogs>
    </>
  );
}
