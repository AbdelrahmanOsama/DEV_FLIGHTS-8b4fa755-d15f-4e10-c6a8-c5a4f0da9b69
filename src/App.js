import React, { Suspense } from "react";
import AppRoutes from "./routes";
import { createTheme, ThemeProvider } from "@mui/material";

import { ToastContainer } from "react-toastify";

// Toastify
import "react-toastify/dist/ReactToastify.css";

function App() {
  const theme = createTheme({
    typography: {
      fontFamily: ["NeoSansArabic"].join(","),
    },
  });

  return (
    <Suspense fallback={"Loading..."}>
      <ThemeProvider theme={theme}>
        <AppRoutes />
        <ToastContainer autoClose={5000} closeButton={false} limit={1} />
      </ThemeProvider>
    </Suspense>
  );
}

export default App;
