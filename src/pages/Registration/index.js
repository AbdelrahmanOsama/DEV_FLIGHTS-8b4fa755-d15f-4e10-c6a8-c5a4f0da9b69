import React from "react";
import RegistrationForm from "components/registration";
import useDocumentTitle from "utils/dynamic-title";
import logo from "assets/images/white-toy-plane-sky.jpeg";

import "./index.scss";

export default function Registration() {
  useDocumentTitle("registration");

  return (
    <div className="register visible_animation">
      <div
        className="flex h-full overflow-y-auto md:overscroll-none"
        spacing={1}
      >
        <div className="flex justify-center login_form w-full lg:w-[60%]">
          <div className="relative z-40 rtl:right-[3%] rtl:left-auto ltr:left-[3%] ltr:right-auto">
            <div className="language_style">
             
            </div>
          </div>
          <RegistrationForm />
        </div>
        <div className="login-background h-[100vh] lg:w-[40%] fixed left-auto right-0 rtl:left-0 rtl:right-auto hidden lg:block">
          <div className="flex justify-center h-full">
            <img className="w-full object-cover" src={logo} alt="background" />
          </div>
        </div>
      </div>
    </div>
  );
}
