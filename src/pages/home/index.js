import React from "react";
import WhiteBox from "components/shared/white-box";
import useDocumentTitle from "utils/dynamic-title";
import Requests from "components/flights/list";

const Home = () => {
  useDocumentTitle("Flights");
  return (
    <div className="flex flex-col h-full w-full gap-6 visible_animation ">
      <WhiteBox className={"visible_animation"}>
        <Requests />
      </WhiteBox>
    </div>
  );
};

export default Home;
