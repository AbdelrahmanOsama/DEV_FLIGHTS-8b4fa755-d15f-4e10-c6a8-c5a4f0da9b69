import React, { useState } from "react";

import CompleteProfileForm from "../../components/complete-profile";

import GlobalLoader from "../../components/shared/Loader/GlobalLoader";

export default function SuccessLoginCallback() {
  const [loading, seLoading] = useState(true);


  if (loading) {
    return <GlobalLoader />;
  } else
    return (
      <div className="h-screen visible_animation">
        <div
          className="flex h-full overflow-y-auto md:overscroll-none"
          spacing={1}
        >
          <div className="flex justify-center  w-full lg:w-[60%]">
            <CompleteProfileForm />
          </div>
          <div className="bg-gradient-to-r from-primary-blue  to-primary-dark/90 h-[100vh] lg:w-[40%] fixed left-auto right-0 rtl:left-0 rtl:right-auto hidden lg:block">
            <div className="flex justify-center h-full">
              <img
                className="w-full object-cover"
                src={"logo"}
                alt="background"
              />
            </div>
          </div>
        </div>
      </div>
    );
}
