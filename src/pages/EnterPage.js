import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import GlobalLoader from "../components/shared/Loader/GlobalLoader";

function EnterPage() {
  const navigate = useNavigate();
  const { user } = useSelector((state) => state.user);

  useEffect(() => {
    if (!user?.id) {
      navigate("/login", {
        replace: true,
      });
    }

    navigate("/login", {
      replace: true,
    });
  }, [navigate, user]);

  return (
    <div className="main visible_animation">
      <GlobalLoader />
    </div>
  );
}
export default EnterPage;
