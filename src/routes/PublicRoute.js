import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Outlet, useNavigate } from "react-router-dom";

const PublicRoute = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const { user } = useSelector((state) => state.user);
  const navigate = useNavigate();

  const goBackToHome = () => {
    navigate("/home?page=1&size=10");
  };

  useEffect(() => {
    const checkAccessToken = () => {
      const session = user?.id;
      if (session) {
        if (session) {
          setIsLoggedIn(true);
        } else {
          setIsLoggedIn(false);
        }
      } else {
        setIsLoggedIn(false);
      }
    };
    checkAccessToken();
  }, [user?.id]);

  return (
    <>
      {!isLoggedIn ? (
        <div className="flex flex-col w-screen h-screen">
          <Outlet />
        </div>
      ) : (
        goBackToHome()
      )}
    </>
  );
};

export default PublicRoute;
