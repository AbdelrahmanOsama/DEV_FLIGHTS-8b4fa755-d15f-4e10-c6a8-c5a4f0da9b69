import { HashRouter, Routes, Route } from "react-router-dom";

// Routes Config
import PrivateRoute from "./PrivateRoute";
import PublicRoute from "./PublicRoute";

// Pages
import Login from "pages/login";
import Registration from "pages/Registration";
import EditFlight from "components/flights/edit";
import Home from "pages/home";
import CreateFlight from "components/flights/create";
import EnterPage from "pages/EnterPage";
import NotFound from "pages/not-found";
import SmoothScroll from "utils/SmoothScroll";
import SuccessPage from "components/flights/success";

export default function AppRoute() {
  return (
    <HashRouter>
      <SmoothScroll>
        <Routes>
          <Route path="/" element={<EnterPage />} />
          <Route element={<PublicRoute />}>
            <Route path="/login" element={<Login />} />
            <Route path="/registration" element={<Registration />} />
          </Route>

          <Route element={<PrivateRoute />}>
            <Route path="/home" element={<Home />} />
            <Route path="/flight/:id" element={<EditFlight />} />
            <Route path="/flight" element={<CreateFlight />} />
            <Route path="/flight/success" element={<SuccessPage />} />
            <Route path="*" element={<NotFound />} />
          </Route>
        </Routes>
      </SmoothScroll>
    </HashRouter>
  );
}
