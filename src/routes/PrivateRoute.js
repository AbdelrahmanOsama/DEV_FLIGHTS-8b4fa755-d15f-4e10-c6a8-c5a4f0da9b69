import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { useMediaQuery } from "react-responsive";
import { Navigate, Outlet } from "react-router-dom";

import Header from "../layout/header";
import Menu from "../layout/side-menu";

const PrivateRoute = () => {
  const isLaptop = useMediaQuery({ minWidth: 1024 });
  const { user } = useSelector((state) => state.user);
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  useEffect(() => {
    if (isMenuOpen && !isLaptop) {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "unset";
    }
  }, [isMenuOpen, isLaptop]);

  if (!user) {
    return <Navigate to="/login" replace />;
  } else {
    return (
      <div className="flex flex-row w-screen min-h-screen visible_animation">
        <Menu isMenuOpen={isMenuOpen} setIsMenuOpen={setIsMenuOpen} />

        <div className="flex min-h-screen flex-col justify-center items-start  w-full lg:fixed-width ">
          <Header setIsMenuOpen={setIsMenuOpen} />
          <div className="flex flex-col flex-1 lg:mt-[124px] lg:px-6 mt-[100px] w-full mb-8">
            <Outlet />
          </div>
          <div className="flex flex-col w-full self-end"></div>
        </div>
      </div>
    );
  }
};

export default PrivateRoute;
