import { combineReducers } from "redux";

import userReducer from "../features/user/userSlice";

const reducer = combineReducers({
  user: userReducer,
});

export default reducer;
