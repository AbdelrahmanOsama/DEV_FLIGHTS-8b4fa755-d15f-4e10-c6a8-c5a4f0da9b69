import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  user: null,
  session: null,
  loadingUserInformation: false,
  getUserInformationError: false,
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUserInfo: (state, { payload }) => {
      state.user = payload;
    },
    setUserSession: (state, { payload }) => {
      state.session = payload;
    },
  },
});

export const { setUserInfo, setUserSession } = userSlice.actions;

export default userSlice.reducer;
