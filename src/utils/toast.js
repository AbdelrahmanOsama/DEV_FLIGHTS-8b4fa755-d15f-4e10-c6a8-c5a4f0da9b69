import { toast, Slide } from "react-toastify";

export default function useToast() {
  const defaultToast = {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: false,
    progress: undefined,
    theme: "colored",
    transition: Slide,
  };
  return {
    toast: (type, message, params) => {
      switch (type) {
        case "success":
          return toast.success(message, { ...defaultToast, params });

        case "error":
          return toast.error(message, { ...defaultToast, params });

        case "info":
          return toast.info(message, { ...defaultToast, params });

        case "warn":
          return toast.warn(message, { ...defaultToast, params });

        default:
          return toast(message, { ...defaultToast, params });
      }
    },
  };
}
