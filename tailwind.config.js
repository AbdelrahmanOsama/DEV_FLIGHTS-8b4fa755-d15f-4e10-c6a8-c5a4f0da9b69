/** @type {import('tailwindcss').Config} */

const withMT = require("@material-tailwind/react/utils/withMT");

module.exports = withMT({
  content: [
    "./src/**/*.{html,js,jsx,ts,tsx}",
    "./node_modules/@material-tailwind/react/components/**/*.{js,ts,jsx,tsx}",
    "./node_modules/@material-tailwind/react/theme/components/**/*.{js,ts,jsx,tsx}",
  ],

  theme: {
    screens: {
      xs: "480px",

      sm: "640px",

      // => @media (min-width: 640px) { ... }

      md: "768px",
      // => @media (min-width: 768px) { ... }

      lg: "1024px",
      // => @media (min-width: 1024px) { ... }

      xl: "1280px",
      // => @media (min-width: 1280px) { ... }

      "2xl": "1536px",
      // => @media (min-width: 1536px) { ... }
    },

    extend: {
      transitionProperty: {
        height: "height",
      },
      colors: {
        "primary-dark": "#002554",
        "primary-blue": "#005EB8",
        "primary-green": "#40C1AC",
        "primary-grey": "#8FA0B4",
        "secondary-grey": "#010101",
        "primary-red": "#C61010",
      },
      backgroundColor: {
        "main-grey": "#F4F6F9",
        "primary-dark": "#002554",
        "primary-blue": "#005EB8",
        "primary-green": "#40C1AC",
        "primary-grey": "#4B4F54",
        "primary-gray": "#8FA0B4",
        "primary-red": "#C61010",
      },

      width: {
        46: "46%",
        140: "140%",
      },
      height: {
        "100vh": "100vh",
      },
    },
  },

  plugins: [require("tailwind-scrollbar")({ nocompatible: true })],
});
